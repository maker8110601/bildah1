#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module contains classes whose purpose is to behave like a simple
struct for collecting hardware declarations.
"""
import dataclasses
from typing import Union, Optional

from bildah1.motoron_interface.constants import PRODUCT_MAP, DEFAULT_ADDRESS
from bildah1.piicodev_interface.constants import PiicoDevType, DEFAULT_ADDRESSES

default_address_space = set(DEFAULT_ADDRESSES.values()) | {DEFAULT_ADDRESS}


@dataclasses.dataclass
class MotoronMotorConfig:
    """
    This class is to capture the configuration of an individual output
    to a motor.

    It is not a control interface.

    All numbers are as derived from information described here:

    https://core-electronics.com.au/guides/raspberry-pi/gobilda-motors-raspberry-pi-pololu-motoron/
    """

    name: str

    # Default Fields
    max_acceleration: int = 300
    max_deceleration: int = 300
    speed_limit: int = 400


@dataclasses.dataclass
class MotoronDevInfo:
    """
    This class is to describe the intent of a motor controller and capture
    its configuration.  It is not a control interface.
    """

    name: str

    # 0 is the RPI, 1 is the first item on the GPIO header, -1 is unknown
    stack_position: int

    # For motoron device codes, see the motoron/scan.py file.
    device: int

    # i2C address
    address: int

    # Optional fields for runtime / polling information
    firmware_version: str = ""
    jumper_status: str = ""
    is_address_set_to_default: bool = False
    v_in_mv: Optional[int] = None
    motor_faulting: Optional[bool] = None
    no_power: Optional[bool] = None
    error_active: Optional[bool] = None
    motor_output_enabled: Optional[bool] = None
    motor_driving: Optional[bool] = None

    command_timeout_milliseconds: int = 10000

    # Other configuration Fields for motor runtime configuration
    motor_1: MotoronMotorConfig = MotoronMotorConfig(name="motor_1")
    motor_2: MotoronMotorConfig = MotoronMotorConfig(name="motor_2")

    def __str__(self):
        """
        This will raise an exception is this is not actually a motoron device
        or on that "looks" like one on the I2C bus.
        """
        product_str = PRODUCT_MAP[self.device]
        return "Addr %3d: product=0x%04X (%s) version=%s JMP1=%s" % (
            self.address,
            self.device,
            product_str,
            self.firmware_version,
            self.jumper_status,
        )

    def might_be_motoron(self) -> bool:
        """
        It might be difficult to get absolute confirmation that a I2C device
        is in fact a motoron device.  The best chance is to check the device code.
        """
        return self.device in PRODUCT_MAP


@dataclasses.dataclass()
class PiicoDevInfo:
    """
    This class describes the intent and configuration of a PiicoDev device.

    It is intended to be immutable.
    """

    type: PiicoDevType
    name: str

    # i2C address
    address: int

    # Optional type specific initial setup
    config: dict = None

    # Optional fields for runtime / polling information
    firmware_version: str = ""
    is_address_set_to_default: bool = False

    def __hash__(self):
        """
        This is not a good idea, it just simplifies the cache code in piicodev_int, this
        can probably be changed in the future.
        """
        return hash(
            (
                self.type,
                self.name,
                self.address,
                self.firmware_version,
                self.is_address_set_to_default,
            )
        )


I2cDeviceInfo = Union[MotoronDevInfo, PiicoDevInfo]
