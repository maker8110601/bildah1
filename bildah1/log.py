import logging
import logging.config

EXTRA_DEBUG = False


def enable_logging(level=logging.DEBUG):
    # Default logging
    # logging.basicConfig(
    #     level=level,
    #     format="%(levelname)-10s  %(asctime)s  %(name)s:%(lineno)d:    %(message)s",
    #     datefmt="%Y-%m-%dT%H:%M:%S",
    # )

    # noinspection SpellCheckingInspection
    logging_config = {
        "disable_existing_loggers": False,
        "version": 1,
        "formatters": {
            "default": {
                "format": "%(levelname)-10s  %(asctime)s  %(name)s:%(lineno)d:    %(message)s",
                "datefmt": "%Y-%m-%dT%H:%M:%S",
            },
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "default",
                "level": level,
            },
        },
        "root": {"handlers": ["console"], "level": level, "propagate": True},
        "loggers": {
            "bildah1.i2c_bus._i2c_thread": {
                "level": "WARNING",
                "propagate": False,
            },
            "asyncio": {
                "level": "WARNING",
                "propagate": False,
            },
        },
    }

    logging.config.dictConfig(logging_config)


def print_to_console(message, interactive=True):
    """
    As logging is mostly used, this is to track where application printing is performed.  Any
    direct print calls in the code will be considered troubleshooting and can be safely removed
    in clean up.

    :param message: The message to print
    :param interactive:  This is used to silence  for convenience
    """
    if interactive:
        print(message)


if __name__ == "__main__":
    enable_logging()

    # Example usage
    logging.debug("This is a debug message")
    logging.info("This is an informational message")
    logging.warning("This is a warning")
    logging.error("This is an error message")
    logging.critical("This is a critical message")
