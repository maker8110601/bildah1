import subprocess

SHUTDOWN_COMMAND = "sudo shutdown now"


def shutdown_system():
    print("Shutting down")
    try:
        # Execute the shutdown command
        subprocess.run(SHUTDOWN_COMMAND.split(" "))
    except Exception as e:
        print(f"System Shutdown command error occurred: {e}")
        raise
