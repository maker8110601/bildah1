#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This module is a private namespace to use within the bildah1.i2c_bus package.

It encapsulates low level interface requests.  This module is intended to
follow a more functional paradigm than an object one.
"""
import functools
import logging
import time
import warnings
from typing import Union, List, Tuple

import cachetools
from PiicoDev_Unified import create_unified_i2c

import bildah1.motoron_interface.identify_device
import bildah1.piicodev_interface.identify_device
from bildah1.hardware_spec_classes import I2cDeviceInfo
from bildah1.i2c_bus.constants import PiicoDevUnifiedTypes
from bildah1.platform_utilities import SimpleRequestQueue, SimpleAsyncResponseQueue

logger = logging.getLogger(__name__)

warnings.warn(
    "This is highly experimental to see how a communications interface"
    " can be set up so that concurrency can be used in the main program.",
    UserWarning,
)


CACHE_TTL = 60
DEBOUNCE_TTL = 1

_i2c_request_queue = SimpleRequestQueue()
_i2c_response_queue = SimpleAsyncResponseQueue()

device_identity_guess_functions = [
    bildah1.motoron_interface.identify_device.test_device_address,
    bildah1.piicodev_interface.identify_device.test_device_address,
]


async def i2c_request(fx_name: Union[str, Tuple[str, str]], *args):
    """
    :raises Exception: If an exception was caught during the request,
        it will propagate up to this function.
    """
    r_id = _i2c_request_queue.put((fx_name, args))
    response = await _i2c_response_queue.wait_for_message_id(r_id)

    return response


@functools.lru_cache
def bus() -> PiicoDevUnifiedTypes:
    return create_unified_i2c()


@cachetools.cached(cache=cachetools.TTLCache(maxsize=8, ttl=CACHE_TTL))
def ping_all_addresses() -> List[Union[int, str]]:
    """
    This is modelled off the motoron libraries where scanning involves
    writing empty data to an address.

    This is not tested in MicroPython.
    """
    logger.info("Scanning for I2C devices...")
    responses = []
    for i in range(1, 128):
        try:
            b = bus()
            b.write8(i, None, b"")
            # .i2c_rdwr(i2c_msg.write(address, []))
            responses.append(i)
        except OSError as e:
            if e.args[0] == 6 or e.args[0] == 121:
                continue
            else:
                msg = f"An error occurred scanning I2C address {i}:  {e}"
                responses.append(msg)
    logger.debug("Returning I2C devices %s", responses)
    return responses


def guess_device_identity(address: int) -> Union[I2cDeviceInfo, bool]:
    """
    This tries its best to work out a device identity by checking
    model and firmware parameters based on each vendor's own
    interfaces.
    """
    if address not in ping_all_addresses():
        raise ValueError(
            f"The address {address} is not in the device scan, "
            f"or a scan has not been performed."
        )

    for identify_fx in device_identity_guess_functions:
        ret = identify_fx(bus(), address)
        if ret is not None:
            return ret
    return False


def reset_caches():
    """
    Reset communication caches
    """
    ping_all_addresses.cache.clear()


def echo(msg: str) -> str:
    """
    Used for manual testing
    """
    time.sleep(2 / 1000)
    return msg


async def echo_request(msg: str) -> str:
    """
    Used for manual testing
    """
    ret = await i2c_request("echo", msg)

    return ret


if __name__ == "__main__":
    print("Running Scan")
    print(ping_all_addresses())
