#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
I bet you were expecting to see a fancy asyncio version of the SMBus class?

At this stage, it is too expensive.  Instead, here are some random functions
that were used to understand how best to interact with the bus along with some
development notes.

Be wary of premature optimisation!

This is a challenge as conceptually an IO bound system works best under
an async paradigm.  How far to take this is unclear.  The goal of this
module is not to undertake extensive premature optimisation but rather create
the technical runway needed if future optimisation is required by encapsulating
key interfaces.

This originally aimed to provide an async interface to the SMBus class, that
is too expensive right now.

While this project is intended initially for use on a Raspberry Pi, it would be
nice to maintain compatibility with MicroPython.  On a Raspberry Pi, it would
be possible to patch in stream readers and writers as long as the SMBus.fd object
is set for non-blocking (O_NONBLOCK).  However, this is not implemented in
micropython.

Basic threading is available as the RPI Pico has two cores.  However, no advanced
functions such as executors etc... exist.

The I2C protocol is deterministic, so if you are coming from a non-deterministic
background, that might take some getting used to.

For the devices currently being used, it seems that a poll cycle is needed for
inputs from an i2c device.  From a quick test, a poll cycle is the magnitude of 1ms
while some basic code (summing 1000 numbers) is in the magnitude of ~10 ms.

So while polling is not slow from a user perspective, it can
"""
import time


def check_blocking_nature_of_i2c():
    """
    The goal of this function is to understand i2c behaviour.  The results will be written
    into the header of this module.


    The read command seems to take about 1.751 ms to execute
    """
    import PiicoDev_RGB
    import PiicoDev_CAP1203

    try:
        leds = PiicoDev_RGB.PiicoDev_RGB(addr=128)
        print(f"ID:  0x{leds.readID():x},  FW: {leds.readFirmware()}")
    except OSError:
        # This fails straight away as no device exists
        pass

    touch_sensor = PiicoDev_CAP1203.PiicoDev_CAP1203()
    start = time.time_ns()
    status = touch_sensor.read()
    end = time.time_ns()
    print(
        "Touch Pad Status: "
        + str(status[1])
        + "  "
        + str(status[2])
        + "  "
        + str(status[3])
    )
    print(f"Read command was {(end - start)/1000000:0.3f} ms")


def rough_benchmarking_report():
    """
    On RPI:  12.672 ms for time1 and 68.372ms for time2
    On my PC:  4.775 ms for time1 and 13.631ms for time2 (per iteration)
    """
    import timeit

    # Define the setup code
    setup_code = "numbers = list(range(1000))"

    # Define two different commands to be tested
    command1 = "[sum(numbers) for _ in range(1000)]"
    command2 = "[sum([number for number in numbers]) for _ in range(1000)]"

    # Run timeit for each command
    iterations = 100
    time1 = timeit.timeit(stmt=command1, setup=setup_code, number=iterations)
    time2 = timeit.timeit(stmt=command2, setup=setup_code, number=iterations)

    print(f"Time using built-in sum: {time1/iterations*1000: 0.3f} ms per iteration")
    print(
        f"Time using list comprehension: {time2/iterations*1000: 0.3f} ms per iteration"
    )


if __name__ == "__main__":
    check_blocking_nature_of_i2c()
    rough_benchmarking_report()
