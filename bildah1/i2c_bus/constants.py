# Importing from upstream is okay, but no importing from Bildah1
# Otherwise race conditions will occur
from typing import Union

from PiicoDev_Unified import I2CUnifiedMachine, I2CUnifiedLinux

PiicoDevUnifiedTypes = Union[I2CUnifiedMachine, I2CUnifiedLinux]

DEFAULT_STACK_POSITION = -1
