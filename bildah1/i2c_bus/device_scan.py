#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This module provides a convenient device scanning interface the
I2C bus for known devices.

It will also be the first prototype in developing an asyncio interface
for communications.  While this is not targeted for MicroPython, the
MicroPython docs will be reviewed while developing this to minimise
future rework.

.. NOTE::
    Rather than use a singleton pattern, this module acts a little like
    a class.
"""
import asyncio
import logging
from typing import Union, List, Optional

from bildah1.hardware_spec_classes import I2cDeviceInfo
from bildah1.i2c_bus import _i2c_interface
from bildah1.log import enable_logging

logger = logging.getLogger(__name__)


async def ping_all_addresses(reset_caches=False) -> List[Union[int, str]]:
    """
    Scans all addresses returning a list of found addresses or an error.

    Note that the scan function is cached to prevent excessive polling.

    :return:  A list of either the device address or an error message.
    """
    logger.debug("ping_all_addresses, this can affect the I2C bus")
    if reset_caches:
        _i2c_interface.reset_caches()

    return await _i2c_interface.i2c_request("ping_all_addresses")


async def guess_device_identity(
    address: int, reset_caches=False
) -> Optional[I2cDeviceInfo]:
    """
    For the given address, returns the information object for the device.

    If no device exists, None is returned.
    """
    if reset_caches:
        _i2c_interface.reset_caches()

    ret = await _i2c_interface.i2c_request("guess_device_identity", address)
    if ret:
        return ret
    else:
        return None


async def scan_for_devices() -> dict[int, I2cDeviceInfo]:
    """
    Scans all addresses and attempts to identify devices at each address.  Any
    errors will be logged and ignored otherwise.
    """

    ret_data = {}

    try:
        address_list = await ping_all_addresses()
    except Exception as e:
        logger.warning("Exception occurred while scanning for addresses", exc_info=e)
        return ret_data

    for dev_addr in address_list:
        if isinstance(dev_addr, str):
            logger.warning("Ignoring a device error: %s", dev_addr)
            continue

        try:
            dev_info = await guess_device_identity(dev_addr)
        except Exception as e:
            logger.warning(
                "Exception occurred while identifying device at %s",
                dev_addr,
                exc_info=e,
            )
            continue

        if dev_info is None:
            logger.warning("Unknown device at: %s", dev_addr)
        else:
            ret_data[dev_addr] = dev_info

    return ret_data


async def refresh_devices(
    dev_info: dict[int, I2cDeviceInfo]
) -> dict[int, I2cDeviceInfo]:
    """
    Returns the actual devices found on the network with refreshed information.
    """
    ret_data = {}

    try:
        address_list = list(dev_info.keys())
    except Exception as e:
        logger.warning("Exception occurred while scanning for addresses", exc_info=e)
        return ret_data

    logger.debug("Refreshing the identity of %i devices", len(address_list))
    for dev_addr in address_list:
        dev_info = await guess_device_identity(dev_addr)
        ret_data[dev_addr] = dev_info

    return ret_data


async def _main():
    """
    Used for prototyping and manual testing of the I2C interface
    """
    from bildah1.i2c_bus._i2c_thread import run_i2c_thread

    # noinspection PyArgumentList
    async with run_i2c_thread():
        logger.info("Waiting for device scan")
        device_list = await ping_all_addresses()
        print(f"Found Device List:  {device_list}")

        dev_data = await scan_for_devices()
        for dev_addr, dev_info in dev_data.items():
            print(f"{dev_addr}: {dev_info}")


if __name__ == "__main__":
    enable_logging()
    asyncio.run(_main(), debug=True)
