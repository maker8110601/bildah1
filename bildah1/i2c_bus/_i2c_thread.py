#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This module is a private namespace to use within the bildah1.i2c_bus package.

It encapsulates the communications thread with the sole focus on thread
management and reliability.
"""
import _thread
import asyncio
import logging
import warnings
from contextlib import asynccontextmanager
from typing import Optional

from bildah1.i2c_bus import _i2c_interface, _vendor
from bildah1.log import enable_logging
from bildah1.platform_utilities import (
    async_sleep_ms,
    SimpleRequestQueue,
    SimpleAsyncResponseQueue,
)

logger = logging.getLogger(__name__)

_i2c_thread = None
_i2c_thread_exception: Optional[RuntimeError] = None
THREAD_REQUEST_TIMEOUT_SECONDS = 3
EXTRA_DEBUGGING = True

# Booleans are threadsafe.
_abort_thread = False
_thread_aborted = False


def _extra_debug_log(*args, **kwargs):
    if EXTRA_DEBUGGING:
        logger.debug(*args, **kwargs)


def _i2c_dispatcher_fx(request):
    """
    The request contains a function name to execute and then function
    arguments.  This works by retrieving the name, looking for the
    function (getattr), assigning the function to a local variable (fx)
    and then executing that function.
    """
    fx_name, fx_args = request
    if isinstance(fx_name, tuple):
        fx_module = getattr(_vendor, fx_name[0])
        fx_name = fx_name[1]
    else:
        fx_module = _i2c_interface

    fx = getattr(fx_module, fx_name)
    try:
        response = fx(*fx_args)
    except Exception as e:
        response = e
    return response


def _i2c_dispatcher_thread():
    """
    This function is run in its own thread that is responsible for
    all I2C bus communications.
    """
    global _i2c_thread_exception, _abort_thread, _thread_aborted
    if _thread_aborted:
        logger.error("The aborted flag has still been set")

    # total_loops = 0
    # total_duration = 0

    try:
        _extra_debug_log("_i2c_dispatcher_thread started")

        # noinspection PyProtectedMember
        for msg_id, request in _i2c_interface._i2c_request_queue.get_generator(
            THREAD_REQUEST_TIMEOUT_SECONDS
        ):
            # total_loops += 1
            # start_time = time.perf_counter_ns()
            if _abort_thread:
                break
            elif msg_id is None:
                continue  # This was a timeout to allow checking for exit conditions
            response = _i2c_dispatcher_fx(request)
            # noinspection PyProtectedMember
            _i2c_interface._i2c_response_queue.put(msg_id, response)
            # total_duration += time.perf_counter_ns() - start_time

        _extra_debug_log("_i2c_dispatcher_thread started")
    except Exception as e:
        logger.exception(e)
        _i2c_thread_exception = RuntimeError(
            "An unhandled exception occurred in the i2c thread, check logs for details."
        )
    # noinspection PyProtectedMember
    _i2c_interface._i2c_response_queue.abort()
    # _extra_debug_log(
    #     f"_i2c_dispatcher_thread executed {total_loops} times for {total_duration} ns, "
    #     f"or about {total_duration/total_loops/1000000:0.3f} ms per loop"
    # )
    _thread_aborted = True


def start_i2c_thread(loop):
    """
    This should be called within an asyncio context.

    :raises RuntimeError: If a thread is running or a past exceptions has not been reset.
    """
    global _i2c_thread, _i2c_thread_exception, _abort_thread, _thread_aborted

    # noinspection PyProtectedMember
    _i2c_interface._i2c_response_queue.set_loop(loop)

    if _i2c_thread is not None:
        logger.error(
            "The I2C thread has been requested multiple times, an exception is being set"
        )
        raise RuntimeError("The I2C thread has been requested multiple times")
    elif _i2c_thread_exception:
        logger.error(
            "An exception has previously been set for the i2c thread.  This must be reset before"
            "a new thread will launch."
        )
        raise _i2c_thread_exception

    _abort_thread = False
    _thread_aborted = False
    logger.info("Starting the I2C thread")
    _i2c_thread = _thread.start_new_thread(_i2c_dispatcher_thread, ())


def stop_i2c_thread():
    global _abort_thread
    logger.info("Signaling for the I2C thread to exit")
    _abort_thread = True


def reset_i2c_thread():
    """
    Used as an interim when the I2C thread is being used synchronously until
    the appropriate design pattern is decided in v2.0
    """
    global _abort_thread, _i2c_thread, _thread_aborted, _i2c_thread_exception
    assert _i2c_thread is None or _thread_aborted
    warnings.warn(
        "Resetting the I2C thread is an experimental procedure only.", UserWarning
    )
    _i2c_thread = None
    _thread_aborted = False
    _abort_thread = False
    _i2c_thread_exception = None
    _i2c_interface._i2c_request_queue = SimpleRequestQueue()
    _i2c_interface._i2c_response_queue = SimpleAsyncResponseQueue()


async def wait_for_thread():
    global _thread_aborted
    while not _thread_aborted:
        logger.debug("Waiting for I2C thread to exit")
        await async_sleep_ms(int(THREAD_REQUEST_TIMEOUT_SECONDS * 1000))
    logger.info("I2C thread has ended")


@asynccontextmanager
async def run_i2c_thread():
    logger.info("Starting I2C thread Context.")
    start_i2c_thread(asyncio.get_running_loop())
    try:
        yield
    finally:
        stop_i2c_thread()
    await wait_for_thread()


async def _dummy_echo(msg):
    """
    With artificial communications time
    """
    ret = _i2c_dispatcher_fx(("echo", (msg,)))
    return ret


async def _overhead_check():
    """
    A rough check on the overhead of the request / response proces.

    Per Call:
    Calling Echo directly is < us
    Awaiting _dummy_echo ~0.003 ms per transaction
    Thread communication ~0.7 ms per transaction
    """
    import time

    iterations = 500
    msg = "Hello"
    msg_list = [msg for _ in range(iterations)]

    with_thread = False
    with_async = True
    as_batch = True

    loop = asyncio.get_running_loop()
    logger.debug(f"Creation Loop: {loop} {hash(loop)}")
    logger.info(f"Overhead check:  {with_thread} {with_async} {as_batch}")

    # noinspection PyArgumentList
    async with run_i2c_thread():
        logger.info("Timing I2C thread request and response cycles")
        start = time.perf_counter()

        if as_batch:
            if with_thread:
                tasks = [_i2c_interface.echo_request(msg) for _ in range(iterations)]
                ret = await asyncio.gather(*tasks)
                assert ret == msg_list
            elif with_async:
                tasks = [_dummy_echo(msg) for _ in range(iterations)]
                ret = await asyncio.gather(*tasks)
                assert ret == msg_list
            else:
                ret = [_i2c_interface.echo(msg) for _ in range(iterations)]
                assert ret == msg_list

        else:
            for _ in range(iterations):
                if with_thread:
                    ret = await _i2c_interface.echo_request(msg)
                elif with_async:
                    ret = await _dummy_echo(msg)
                else:
                    ret = _i2c_dispatcher_fx(("echo", (msg,)))
                assert ret == msg, ret
        end = time.perf_counter()
    duration = end - start
    duration_per = duration / iterations
    logger.info(
        "Total time duration is %0.2f seconds for %i iterations", duration, iterations
    )
    logger.info("Total time duration is %0.3f ms per iteration", duration_per * 1000)


if __name__ == "__main__":
    enable_logging()

    def _main():
        # import asyncio
        # import selectors
        #
        # selector = selectors.EpollSelector()
        # loop = asyncio.SelectorEventLoop(selector)
        # asyncio.set_event_loop(loop)
        # loop = asyncio.get_event_loop()
        # logger.debug(f"Creation Loop: {loop} {hash(loop)}")
        # asyncio.set_event_loop(loop)
        asyncio.run(_overhead_check(), debug=True)

    _main()
