from bildah1.i2c_bus._i2c_interface import i2c_request
from bildah1.i2c_bus._i2c_thread import run_i2c_thread, reset_i2c_thread


def validate_address(address: int):
    """
    :raises ValueError: On invalid address
    """
    if 0 < address <= 127:
        return
    else:
        raise ValueError(f"Invalid address: {address}")
