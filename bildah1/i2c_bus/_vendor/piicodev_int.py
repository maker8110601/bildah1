#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module is a communication interface to the Bildah1 i2c bus interface.
"""
import functools
import logging
from typing import Union, Dict

import PiicoDev_RGB

from bildah1.hardware_spec_classes import PiicoDevInfo
from bildah1.i2c_bus._i2c_interface import bus, guess_device_identity
from bildah1.piicodev_interface.constants import PiicoDevType

SUPPORTED_PIICODEV_CLASSES = (PiicoDev_RGB.PiicoDev_RGB,)
PIICODEV_TYPE = Union[PiicoDev_RGB.PiicoDev_RGB]

logger = logging.getLogger(__name__)

EXTRA_DEBUG = True


class Dummy:
    def __init__(self, i2c=None, addr=None):
        self.i2c = i2c
        self.addr = addr


def set_address(device_type: PiicoDevType, from_address: int, to_address: int) -> bool:
    """
    Addressing settings will depend on the device being requested.  In most cases the
    PiicoDev device class will not be instantiated and a Dummy class will be used to get
    the methods to work like static methods.
    """
    i2c = bus()
    from_device = guess_device_identity(from_address)

    if from_device.type != device_type:
        raise ValueError(
            f"Asking to change the address of {from_address}:{device_type} for a different type.  "
            f"the current device is a {from_device.type}"
        )

    if device_type == PiicoDevType.RGB:
        dev = PiicoDev_RGB.PiicoDev_RGB
        dummy = Dummy(i2c=i2c, addr=from_address)
        # noinspection PyTypeChecker
        dev.setI2Caddr(dummy, to_address)

    else:
        raise NotImplementedError(
            f"Setting address for PiicoDev device {device_type} has not been implemented yet."
        )

    return True


@functools.lru_cache(maxsize=None)
def get_device(dev: PiicoDevInfo) -> PIICODEV_TYPE:
    """
    Instantiates a device corresponding to the Device Info class.  This
    function is cached to allow return of the same class.  Any startup
    bildah1 configuration of the device is also performed here.
    """
    if dev.type == PiicoDevType.RGB:
        brightness = 50
        if dev.config:
            brightness = dev.config.get("brightness", brightness)

        new_dev = PiicoDev_RGB.PiicoDev_RGB(addr=dev.address, bright=brightness)

    else:
        raise NotImplementedError(
            f"PiicoDev device {dev} has not been implemented yet."
        )

    return new_dev


def set_rgb_pixels(dev_info: PiicoDevInfo, pixel_rgb: Dict[int, tuple]) -> bool:
    """
    Sets the pixels of the given RGB Device
    """
    if dev_info.type != PiicoDevType.RGB:
        raise ValueError(f"Incorrect device for set_rgb_pixels:  {dev_info} ")

    rgb_dev: PiicoDev_RGB.PiicoDev_RGB = get_device(dev_info)

    for led_index, rgb in pixel_rgb.items():
        rgb_dev.setPixel(led_index, rgb)

    rgb_dev.show()

    return True
