#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module is a communication interface to the Bildah1 i2c bus interface.
"""
import functools
import logging
import warnings

import motoron

from bildah1.i2c_bus._i2c_interface import bus
from bildah1.platform_utilities import PLATFORM_IS_LINUX

logger = logging.getLogger(__name__)


@functools.lru_cache
def get_motoron_i2c(address=0):
    if not PLATFORM_IS_LINUX:
        warnings.warn("Motoron is not tested on other platforms yet.", UserWarning)
    unified_bus = bus()
    mc = motoron.MotoronI2C(bus=unified_bus.i2c, address=address)
    return mc


def reset(address: int) -> bool:
    """
    Uses the MotoronI2C driver to set the address of the requested device.
    This assumes the input addresses are correct.
    """
    mc = get_motoron_i2c(address)
    mc.enable_crc()
    logger.info("Sending command to reset motoron at address %i", address)
    mc.reset(ignore_nack=False)

    return True


def set_address(from_address: int, to_address: int) -> bool:
    """
    Uses the MotoronI2C driver to set the address of the requested device.
    This assumes the input addresses are correct.
    """
    mc = get_motoron_i2c()
    mc.enable_crc()
    # Send a command to set the EEPROM device number to the desired
    # address.  This command will only be received by Motoron devices that
    # are configured to listen to the general call address (which is
    # the default) and will only be obeyed by Motoron devices that have JMP1
    # shorted to GND.
    logger.info("Sending command to change address %i -> %i", from_address, to_address)
    mc.write_eeprom_device_number(to_address)

    return True
