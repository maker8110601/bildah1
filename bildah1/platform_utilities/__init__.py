#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This module captures platform specific interfaces and common functions.

It is important that this module only imports from standard libraries
available cross-platform and not from any other modules.

Some code was inspired by the PiicoDev Unified libraries as well as the
Pololu libraries.

Note that this module is occasionally tested on an RPI Pico to check
for interpreter capabilities (especially type hinting compatibility).
"""
from bildah1.platform_utilities.current_platform import (
    PLATFORM_IS_LINUX,
    PLATFORM_IS_RPI_PICO,
    _uname,
    PLATFORM_IS_UNKNOWN,
)
from bildah1.platform_utilities.simple_request_queue import (
    SimpleRequestQueue,
    SimpleAsyncResponseQueue,
)

if PLATFORM_IS_LINUX:
    from time import sleep as _sleep, time as _time_s
    from asyncio import sleep as _a_sleep

    def sleep_ms(t: int) -> None:
        _sleep(t / 1000)

    async def async_sleep_ms(t: int) -> None:
        return await _a_sleep(t / 1000)

    def ticks_ms() -> int:
        """
        A CPython implementation of
        https://docs.micropython.org/en/v1.8.7/esp8266/library/utime.html#utime.ticks_ms
        """
        return int(_time_s() * 1000)

    def ticks_diff(t1: int, t2: int) -> int:
        """
        https://docs.micropython.org/en/v1.8.7/esp8266/library/utime.html#utime.ticks_ms
        """
        return t1 - t2

elif PLATFORM_IS_RPI_PICO:
    print(f"Found a Pico: {_uname}")

    # Note that utime on a PICO is not the same as utime in PyPI
    # noinspection PyUnresolvedReferences
    from utime import sleep_ms, ticks_ms, ticks_diff

    # noinspection PyUnresolvedReferences
    from asyncio import sleep_ms as async_sleep_ms


else:
    import os

    raise NotImplementedError(f"Unsupported platform: {os.uname()}")


def _platform_report() -> None:
    # noinspection SpellCheckingInspection
    """
    This function is intended to be run on platforms to retrieve the
    relevant identifiers.

    This was tested this on an RPI Pico W, and I needed to add my user
    to the dialout group (on my PC) which in console was ``sudo groupmod -aU gary dialout``.

    After a restart, I used the micropython plugin to Pycharm to access REPL on /dev/ttyACM0 (set
    the device path in PyCharm settings, search for MicroPython settings).

    Sometimes that didn't work, so going to a console with ``screen /dev/ttyACM0 115200`` is a
    useful backup as well as ``mpremote repl`` (see below for mpremote install).

    If the "Flash" option of the micropython plugin does not work as a run configuration, adding
    this file manually requires installing mpremote.  This can be done as::

        # Assuming pipx is installed on your development PC
        pipx install mpremote
        cd bildah1
        mpremote --help
        # mpremote a0
        mpremote run platform_utilities.py

    The output of this was::

        (sysname='rp2', nodename='rp2', release='1.22.1',
        version='v1.22.1 on 2024-01-05 (GNU 13.2.0 MinSizeRel)',
        machine='Raspberry Pi Pico W with RP2040'
        )

    """
    try:
        uname = os.uname()
        print(uname)
    except Exception as e:
        print("Uname does not exist")
        print(e)

    print("Sleeping")
    sleep_ms(500)
    print("Done")
    print(help("modules"))


if __name__ == "__main__":
    _platform_report()
