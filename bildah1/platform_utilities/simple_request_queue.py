#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This module provides Queue objects that can be used in asyncio and thread safe
ways.
"""
import _thread
import asyncio
import logging
from asyncio import Event as AsyncEvent

from bildah1.exceptions import is_exception
from bildah1.platform_utilities.current_platform import (
    PLATFORM_IS_LINUX,
    PLATFORM_IS_RPI_PICO,
)
from bildah1.python_compat import NumberType

if PLATFORM_IS_LINUX:
    from threading import Event as ThreadEvent
elif PLATFORM_IS_RPI_PICO:
    raise NotImplementedError(
        "This is the place for a micropython equivalent of Event is needed (not a port, just "
        "api compatible)."
    )
else:
    import os

    raise NotImplementedError(f"Unsupported platform: {os.uname()}")


EXTRA_DEBUGGING = False

logger = logging.getLogger(__name__)


def _extra_debug_log(*args, **kwargs):
    if EXTRA_DEBUGGING:
        logger.debug(*args, **kwargs)


class SimpleRequestQueue:
    """
    This queue is intended to be thread safe request queue where a non-blocking
    request is added to the queue and given an ID number.  The response is not
    provided here but in SimpleResponseQueue.
    """

    MAX_MSG_NO = 1000000
    MAX_QUEUE_SIZE = 10000

    def __init__(self):
        self.queue = []
        self._last_msg_no = 0
        self.lock = _thread.allocate_lock()
        self.thread_updated_event = ThreadEvent()

    def _increment_msg_no(self) -> int:
        """
        :return: A message number that loops between 0 and MAX_MSG_NO.
        """
        self._last_msg_no = (
            (self._last_msg_no + 1) if self._last_msg_no < self.MAX_MSG_NO else 0
        )
        return self._last_msg_no

    def put(self, item: tuple) -> int:
        """
        Called from the main thread in async code.
        :raises OverflowError:  On full queue
        """
        with self.lock:
            if len(self.queue) > self.MAX_QUEUE_SIZE:
                raise OverflowError("Queue is too full")
            msg_id = self._increment_msg_no()
            self.queue.append((msg_id, item))
            self.thread_updated_event.set()
            return msg_id

    def get_generator(self, loop_timeout_seconds: NumberType = None):
        while True:
            with self.lock:
                if self.queue:
                    msg_id, request = self.queue.pop(0)
                else:
                    msg_id = None
                    request = None

            yield msg_id, request

            with self.lock:
                if self.queue:
                    more_messages = True
                else:
                    more_messages = False
                    self.thread_updated_event.clear()

            if not more_messages:
                _extra_debug_log("Waiting for requests")
                self.thread_updated_event.wait(loop_timeout_seconds)


class SimpleAsyncResponseQueue:
    """
    This queue is the sibling of the RequestQueue.  It is intended to be
    instantiated from the main async loop.  This does not support asyncio event
    loops existing in other threads.
    """

    MAX_QUEUE_SIZE = SimpleRequestQueue.MAX_QUEUE_SIZE

    def __init__(self):
        self.queue = {}
        self.lock = _thread.allocate_lock()
        self.async_updated_event = None
        self.loop = None
        self.aborted = False
        self.cancelled_requests = []

    def set_loop(self, loop):
        self.loop = loop
        self.async_updated_event = AsyncEvent()

    def put(self, msg_id: int, response):
        """
        Called in a thread

        :raises ValueError:  if the response is None
        :raises RuntimeError:  On any message related error
        :raises OverflowError:  On full queue
        """
        if not self.loop:
            raise RuntimeError(
                "The Response queue has not been initialised correctly, "
                "please call the set_loop() method from within the asyncio loop."
            )
        if response is None:
            raise ValueError("A response can not be None")
        with self.lock:
            if len(self.queue) > self.MAX_QUEUE_SIZE:
                raise OverflowError("Queue is too full")
            elif msg_id in self.queue:
                raise RuntimeError("Duplicate message id exists!")
            self.queue[msg_id] = response
        self.loop.call_soon_threadsafe(self.async_updated_event.set)
        return msg_id

    def abort(self):
        """
        Something has gone wrong and the queue needs to close, terminating
        any wait request.
        """
        with self.lock:
            self.aborted = True
            self.loop.call_soon_threadsafe(self.async_updated_event.set)

    async def wait_for_message_id(self, msg_id: int):
        """
        :raises RuntimeError:  If the queue needs to be aborted
        :raises Exception:  If the request raised an exception, the exception
        will be raised again here
        :returns: The response from the called function
        """
        # Note that the next command will block this thread, but the
        # put command is meant to be very light-weight so shouldn't
        # be a problem.
        while True:
            if self.aborted:
                raise RuntimeError("The queue has aborted")

            with self.lock:
                response = self.queue.pop(msg_id, None)
                if response:
                    _extra_debug_log(f"{msg_id} Response received: {response}")

                # Clean up any past cancelled requests while here
                if self.cancelled_requests:
                    popped_requests = []
                    for _m_id in self.cancelled_requests:
                        if _m_id in self.queue:
                            popped_requests.append(_m_id)
                    for _m_id in popped_requests:
                        del self.queue[_m_id]

            # Check if an error has been raised, then process the response
            if is_exception(response):
                raise response
            elif response is not None:
                return response
            else:
                with self.lock:
                    self.async_updated_event.clear()

                try:
                    await self.async_updated_event.wait()
                    # await asyncio.sleep(0)
                except asyncio.CancelledError:
                    # Waiting for a message that will be cancelled
                    # Something has gone waiting, the message will never be
                    # removed from the queue.
                    self.cancelled_requests.append(msg_id)
                    # break
                    raise
