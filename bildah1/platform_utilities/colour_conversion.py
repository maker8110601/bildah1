import colorsys


def hsv_to_rgb(h, s=1, v=1, hue_is_in_degrees=True, rgb_max=255, conversion_fx=int):
    """
    A very flexible color converter to assist with different vendor colour interfaces.

    :param h: The value for hue, either 0-1 or 0-360 (see other arguments)
    :param s: Saturation value 0 - 1
    :param v: Value 0 - 1
    :param hue_is_in_degrees:  True if the hue is in degrees and will be interpreted 0 to 360
    :param rgb_max:  This is used to convert 0 to 1 into an RGB value
    :param conversion_fx:  Each value is after scaling is passed to this function for cleaning
    :return: RGB values on a range of 0 - rgb_max
    """
    if hue_is_in_degrees:
        h = h / 360
    return tuple(conversion_fx(x * rgb_max) for x in colorsys.hsv_to_rgb(h, s, v))


def rgb_to_hsv(rgb: tuple, rgb_max=255):
    """
    A very flexible color converter to assist with different vendor colour interfaces.

    :param rgb: A tuple of (red, green, blue)
    :param rgb_max:  This is used to convert 0 to 1 into an RGB value
    """
    normalised_rgb = tuple(x / rgb_max for x in rgb)
    return tuple(x for x in colorsys.rgb_to_hsv(*normalised_rgb))
