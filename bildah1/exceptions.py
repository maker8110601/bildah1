#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
All custom exception classes occur here.  Typically, exceptions will indicate what type and
severity of problem occurred, however, these exceptions go a little further by indicating
what appropriate restoration strategies can be used.
"""


class BildahCriticalException(RuntimeError):
    """
    This is raised to indicate that an unrecoverable error has occurred.
    """


class BildahExceptionMixin:
    """
    Used to add recovery information to exceptions
    """

    should_recover_on_soft_restart: bool = False
    recommended_wait: int = 1000


class BildahConnectionError(ConnectionError, BildahExceptionMixin):
    """
    Raised based on OS level events such as missing devices / hardware.  Something
    that needs to be restored at the host level.
    """

    should_recover_on_soft_restart = True

    def __init__(
        self,
        msg: str,
        recommended_wait: int = 1000,
    ):
        self.recommended_wait = recommended_wait
        super().__init__(msg)


class BildahCriticalConnectionError(BildahConnectionError):
    """
    This class indicates a possible error that may not be recoverable automatically.
    If this error is raised too many times, the expectation is that the main program
    will fall to an error state or shutdown.
    """

    remaining_error_count = 5
    recommended_wait = 5000

    def __init__(
        self,
        msg: str,
    ):
        if BildahCriticalConnectionError.remaining_error_count > 0:
            super().__init__(msg, recommended_wait=self.recommended_wait)
        else:
            raise BildahCriticalException(
                f"A critical error has occurred multiple times.  The last error was: {msg}"
            )

    @classmethod
    def reset_error_count(cls):
        cls.remaining_error_count = 5


def is_exception(obj) -> bool:
    """
    Sometimes exception classes are raised without being an instance.
    """
    return (isinstance(obj, type) and issubclass(obj, Exception)) or isinstance(
        obj, Exception
    )
