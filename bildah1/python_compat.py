"""
Constants use to assist with compatibility where older syntax might
not be as readable.
"""
from typing import Union

NumberType = Union[int, float]
