#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module provides a Bildah1 interface to a 3 LED RGB Module.
"""
import asyncio
import logging
import math
import warnings

from bildah1.hardware_spec_classes import PiicoDevInfo
from bildah1.i2c_bus import i2c_request
from bildah1.piicodev_interface.constants import PiicoDevType
from bildah1.platform_utilities import async_sleep_ms, ticks_ms, ticks_diff
from bildah1.platform_utilities.colour_conversion import rgb_to_hsv, hsv_to_rgb

logger = logging.getLogger(__name__)

RGB_OFF = (0, 0, 0)
EXTRA_DEBUG = False


class BaseLightSequence:
    """
    A common interface for all the light sequences.  A sequence
    operates as a background task. Subclasses must implement the
    run method for the sequence and update the value variable.
    """

    def __init__(self, runs_forever=True, max_brightness=0.5):
        self.runs_forever = runs_forever
        self.max_brightness = max_brightness
        self.value = None

    async def run(self):
        """
        To be overridden by child classes
        """
        raise NotImplementedError


class OnOffSequence(BaseLightSequence):
    """
    Note:  This is not a synchronised sequence.  The timing events
    will only be sent when the available and do not account for other
    program execution times.

    Used for on and off operation of a single LED, can also be used
    to just set on / leave on or switch colours.

    LEDs keep their last state when the sequence finishes.

    A light sequence is True/False and duration in milliseconds.

    For example:
    [
        (False, 300),
        (True, 500),
        (False, 300),
    ]

    """

    def __init__(
        self,
        sequence: list,
        repeat=True,
        rgb=(255, 255, 255),
        max_brightness=0.5,
    ):
        super().__init__(runs_forever=repeat is True, max_brightness=max_brightness)
        self.sequence = sequence
        self.repeat = repeat
        self.rgb = rgb
        self.value = RGB_OFF

    async def run(self):
        sequence_step = 0
        remaining_repeats = self.repeat - 1

        while True:
            light_val, duration_ms = self.sequence[sequence_step]

            self.value = self.rgb if light_val else RGB_OFF

            await async_sleep_ms(duration_ms)

            sequence_step += 1
            if sequence_step >= len(self.sequence):
                # Sequence has ended
                if self.runs_forever:
                    sequence_step = 0
                elif remaining_repeats > 0:
                    remaining_repeats -= 1
                    sequence_step = 0
                else:
                    break


class CosineWaveSequence(BaseLightSequence):
    """
    Note:  This is not a synchronised sequence.  The timing events
    will only be sent when the available and do not account for other
    program execution times.

    Used to pulsate a light using a sine wave.  The default sine wave
    will move from 0 to 1.

    LEDs keep their last state when the sequence finishes / is cancelled.
    """

    def __init__(
        self,
        period_ms=1000,
        phase_shift_ms=0,
        amplitude=0.5,
        offset=0.5,
        start_low=True,
        update_rate=100,
        repeat_ms=True,
        rgb=(255, 255, 255),
        max_brightness=0.5,
    ):
        super().__init__(runs_forever=repeat_ms is True, max_brightness=max_brightness)
        self.period_ms = period_ms
        self.phase_shift_ms = phase_shift_ms
        self.amplitude = amplitude
        self.offset = offset
        self.start_low = start_low
        self.update_rate = update_rate
        self.repeat_ms = repeat_ms
        self.rgb = rgb
        self.value = RGB_OFF
        self.start_ticks_ms = None

    async def run(self):
        self.start_ticks_ms = ticks_ms()
        sign = -1 if self.start_low else 1

        initial_hsv = rgb_to_hsv(self.rgb)

        while True:
            x = ticks_diff(self.start_ticks_ms, ticks_ms())
            y = (
                sign
                * self.amplitude
                * math.cos(2 * math.pi * (x + self.phase_shift_ms) / 1000)
                + self.offset
            )
            y = max(min(y, 1), 0)

            new_v = y * initial_hsv[2]

            self.value = hsv_to_rgb(
                initial_hsv[0], initial_hsv[1], new_v, hue_is_in_degrees=False
            )

            await async_sleep_ms(self.update_rate)
            if x > self.repeat_ms:
                break


async def run_light_sequence(
    dev_info: PiicoDevInfo,
    sequence_0: BaseLightSequence = None,
    sequence_1: BaseLightSequence = None,
    sequence_2: BaseLightSequence = None,
    update_rate_ms: int = 100,
):
    """
    Runs the given light sequence for each LED until the sequence ends (or forever).
    """

    warnings.warn(
        "Whether the sequence is configured in the main thread or"
        " communications thread is still to be decided, expect this behaviour to change.",
        UserWarning,
    )
    assert dev_info.type == PiicoDevType.RGB

    sequences = {}
    running_tasks = []
    if sequence_0 is not None:
        sequences[0] = sequence_0
        running_tasks.append(asyncio.create_task(sequence_0.run()))
    if sequence_1 is not None:
        sequences[1] = sequence_1
        running_tasks.append(asyncio.create_task(sequence_1.run()))
    if sequence_2 is not None:
        sequences[2] = sequence_2
        running_tasks.append(asyncio.create_task(sequence_2.run()))

    while True:
        led_commands = {led_n: seq.value for led_n, seq in sequences.items()}
        await i2c_request(("piicodev", "set_rgb_pixels"), dev_info, led_commands)
        await async_sleep_ms(update_rate_ms)
        if all(s.done() for s in running_tasks):
            break


async def leds_off(
    dev_info: PiicoDevInfo,
):
    assert dev_info.type == PiicoDevType.RGB
    await i2c_request(
        ("piicodev", "set_rgb_pixels"),
        dev_info,
        {
            0: RGB_OFF,
            1: RGB_OFF,
            2: RGB_OFF,
        },
    )


async def leds_on(dev_info: PiicoDevInfo, rgb):
    assert dev_info.type == PiicoDevType.RGB
    await i2c_request(
        ("piicodev", "set_rgb_pixels"),
        dev_info,
        {
            0: rgb,
            1: rgb,
            2: rgb,
        },
    )
