#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
from typing import Optional

import PiicoDev_CAP1203
import PiicoDev_RGB

import bildah1.piicodev_interface.overrides
from bildah1.hardware_spec_classes import PiicoDevInfo
from bildah1.i2c_bus.constants import PiicoDevUnifiedTypes
from bildah1.piicodev_interface.constants import DEV_ID_TYPE_MAP, DEV_ID_MAP

logger = logging.getLogger(__name__)

device_tests = [
    (
        PiicoDev_RGB,
        PiicoDev_RGB.PiicoDev_RGB,
        "_DevID",
        "_baseAddr",
        "readID",
        "readFirmware",
    ),
    (
        PiicoDev_CAP1203,
        bildah1.piicodev_interface.overrides.PiicoDev_CAP1203,
        "_PROD_ID_VALUE",
        "_CAP1203Address",
        "readID",
        "readFirmware",
    ),
]


# noinspection PyUnusedLocal
def test_device_address(bus: PiicoDevUnifiedTypes, address) -> Optional[PiicoDevInfo]:
    """
    Each PiicoDev device is unique, this can break if there are significant changes
    to the PiicoDev modules.

    Some devices will change state from this scan.
    """

    class Dummy:
        i2c = bus
        addr = address

    for (
        module,
        cls,
        dev_id_name,
        default_address_name,
        id_fx_name,
        fw_fx_name,
    ) in device_tests:
        # Instantiating some PiicoDev devices can cause them to
        # Change state.  Instead, this code will try and bypass the constructor.
        id_fx = getattr(cls, id_fx_name)
        expected_dev_id = getattr(module, dev_id_name)
        if isinstance(expected_dev_id, bytes):
            expected_dev_id = int.from_bytes(expected_dev_id, "big")
        try:
            actual_dev_id = id_fx(Dummy)
            if actual_dev_id == expected_dev_id:
                fw_fx = getattr(cls, fw_fx_name)
                fw_version = fw_fx(Dummy)

                return PiicoDevInfo(
                    type=DEV_ID_TYPE_MAP[actual_dev_id],
                    name=DEV_ID_MAP[actual_dev_id],
                    address=address,
                    firmware_version=str(fw_version),
                )
        except Exception as e:
            logger.warning("Error scanning device at I2C address: %s", address)
            logger.exception(e)
