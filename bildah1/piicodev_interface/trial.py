#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import PiicoDev_RGB

from bildah1.platform_utilities.colour_conversion import hsv_to_rgb


def scan_for_piicodev():
    # Assumes h 0 -> 360, and the other two 0 -> 1
    ini_check_colour_r_g_b = hsv_to_rgb(*[213, 0.85, 1])
    operating_colour_r_g_b = hsv_to_rgb(*[125, 1, 1])
    shutting_down_colour_r_g_b = hsv_to_rgb(*[32, 1, 1])

    current_colour = shutting_down_colour_r_g_b

    leds = PiicoDev_RGB.PiicoDev_RGB(addr=30)
    print(f"ID:  0x{leds.readID():x},  FW: {leds.readFirmware()}")
    leds.fill(current_colour)


if __name__ == "__main__":
    # Note:  On system shutdown: the LEDs retain their colour.
    # an interesting link:  https://stackoverflow.com/questions/62757687/raspberry-pi-4-i2c-bus-not-working-correctly
    # https://www.raspberry-pi-geek.com/Archive/2015/09/Getting-to-know-the-Raspberry-Pi-I2C-bus
    scan_for_piicodev()
