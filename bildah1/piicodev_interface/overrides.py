#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import PiicoDev_CAP1203 as PiicoDev_CAP1203_module


# noinspection PyPep8Naming
class PiicoDev_CAP1203(PiicoDev_CAP1203_module.PiicoDev_CAP1203):
    """
    This override reinstates the ID check as a dedicated function.  Hardware hints are here:

    https://github.com/sparkfun/SparkFun_CAP1203_Arduino_Library/blob/main/src/SparkFun_CAP1203_Registers.h

    Manual here:
    https://ww1.microchip.com/downloads/en/DeviceDoc/00001572B.pdf

    """

    REVISION = 0xFF

    def readID(self):
        # noinspection PyProtectedMember
        product_ID_value = self.i2c.readfrom_mem(
            self.addr, int.from_bytes(PiicoDev_CAP1203_module._PRODUCT_ID, "big"), 1
        )[0]
        return product_ID_value

    # noinspection PyMethodMayBeStatic
    def readFirmware(self):
        v = self.i2c.readfrom_mem(self.addr, PiicoDev_CAP1203.REVISION, 1)
        return v[0]
