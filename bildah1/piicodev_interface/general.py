#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module is a communication interface to the Bildah1 i2c bus interface.
"""
import asyncio
import logging

from bildah1.i2c_bus import validate_address, i2c_request
from bildah1.i2c_bus.device_scan import ping_all_addresses
from bildah1.log import enable_logging
from bildah1.piicodev_interface import constants
from bildah1.piicodev_interface.constants import PiicoDevType

logger = logging.getLogger(__name__)


async def set_piicodev_address(
    device_type: PiicoDevType, from_address: int, to_address: int
):
    """
    Set an i2c address for a PiicoDev device.

    Note that many of the devices had ID switches / bridges that
    can also set the address.  These should be set to their default (off)
    position.

    :raises ValueError: if there is a problem with one of the given addresses
    :raises RuntimeError: if the change did not occur as expected
    :raises Exception:  Other exceptions might be raised that are not handled here.
    :return: If an exception is not raised, the address has been set.
    """
    # Input validation
    validate_address(from_address)
    validate_address(to_address)
    existing_addresses = await ping_all_addresses(reset_caches=True)
    if to_address in existing_addresses:
        raise ValueError(f"Address {to_address} already exists.")
    elif from_address not in existing_addresses:
        raise ValueError(
            f"Address {from_address} does not exist and can not be changed."
        )

        # Update address

    await i2c_request(
        ("piicodev", "set_address"), device_type, from_address, to_address
    )

    # Change Validation
    existing_addresses = await ping_all_addresses(reset_caches=True)
    if from_address in existing_addresses:
        raise RuntimeError(
            f"PiicoDev at address {from_address} did not change, check ID switches/bridges."
        )
    if to_address not in existing_addresses:
        raise RuntimeError(
            f"PiicoDev that was at address {from_address} has changed but not to the expected address {to_address}."
        )


async def _main(device_type: PiicoDevType, from_address: int, to_address: int):
    """
    Used for prototyping and integration testing of address assignment logic
    """
    from bildah1.i2c_bus import run_i2c_thread

    # noinspection PyArgumentList
    async with run_i2c_thread():
        logger.info("Waiting for device scan")

        ret = await set_piicodev_address(device_type, from_address, to_address)

        print(f"Set Address returned with: {ret}")


if __name__ == "__main__":
    enable_logging()

    def _main():
        _dt = PiicoDevType.RGB
        _default_address = constants.DEFAULT_ADDRESSES[str(_dt.value)]
        asyncio.run(_main(_dt, 12, _default_address), debug=True)

    _main()
