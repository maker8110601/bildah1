import enum
import warnings

# noinspection PyUnresolvedReferences
import PiicoDev_BME280
import PiicoDev_Buzzer
import PiicoDev_CAP1203
import PiicoDev_ENS160
import PiicoDev_LIS3DH
import PiicoDev_MPU6050
import PiicoDev_MS5637
import PiicoDev_Potentiometer
import PiicoDev_QMC6310
import PiicoDev_RFID
import PiicoDev_RGB
import PiicoDev_RV3028
# noinspection PyUnresolvedReferences
import PiicoDev_SSD1306
import PiicoDev_Servo
import PiicoDev_Switch
import PiicoDev_TMP117
import PiicoDev_Transceiver
import PiicoDev_Ultrasonic
import PiicoDev_VEML6030
import PiicoDev_VEML6040
# noinspection PyUnresolvedReferences
import PiicoDev_VL53L1X


class PiicoDevType(enum.Enum):
    OLED = "PiicoDev_SSD1306"
    RGB = "PiicoDev_RGB"
    BUZZER = "PiicoDev_Buzzer"
    CAPACITIVE_TOUCH = "PiicoDev_CAP1203"


STR_TO_TYPE_MAP = {_x.value: _x for _x in PiicoDevType.__members__.values()}

# noinspection PyProtectedMember
DEFAULT_ADDRESSES = {
    "PiicoDev_BME280": 0x77,
    "PiicoDev_Buzzer": PiicoDev_Buzzer._baseAddr,
    "PiicoDev_CAP1203": PiicoDev_CAP1203._CAP1203Address,
    "PiicoDev_ENS160": PiicoDev_ENS160._I2C_ADDRESS,
    "PiicoDev_LIS3DH": PiicoDev_LIS3DH._I2C_ADDRESS,
    "PiicoDev_MPU6050": PiicoDev_MPU6050._MPU6050_ADDRESS,
    "PiicoDev_MS5637": PiicoDev_MS5637.PiicoDev_MS5637._I2C_ADDRESS,
    "PiicoDev_Potentiometer": PiicoDev_Potentiometer._BASE_ADDRESS,
    "PiicoDev_QMC6310": PiicoDev_QMC6310._I2C_ADDRESS,
    "PiicoDev_RFID": PiicoDev_RFID._I2C_ADDRESS,
    "PiicoDev_RGB": PiicoDev_RGB._baseAddr,
    "PiicoDev_RV3028": PiicoDev_RV3028._I2C_ADDRESS,
    "PiicoDev_Servo": PiicoDev_Servo._I2C_ADDRESS,
    "PiicoDev_SSD1306": 0x3C,
    "PiicoDev_Switch": PiicoDev_Switch._BASE_ADDRESS,
    "PiicoDev_TMP117": PiicoDev_TMP117._baseAddr,
    "PiicoDev_Transceiver": PiicoDev_Transceiver._BASE_ADDRESS,
    "PiicoDev_Ultrasonic": PiicoDev_Ultrasonic._BASE_ADDRESS,
    "PiicoDev_VEML6030": PiicoDev_VEML6030._veml6030Address,
    "PiicoDev_VEML6040": PiicoDev_VEML6040._veml6040Address,
    "PiicoDev_VL53L1X": 0x29,
}

# noinspection PyProtectedMember
DEVICE_IDS = {
    # Might be able to add when needed:  https://www.mouser.com/datasheet/2/783/BST-BME280_DS001-11-844833.pdf
    # This should be 0x60 and is available on register 0xD0 (P24 of the chip manual)
    "PiicoDev_BME280": None,
    "PiicoDev_Buzzer": PiicoDev_Buzzer._DevID,
    "PiicoDev_CAP1203": PiicoDev_CAP1203._PRODUCT_ID,
    "PiicoDev_ENS160": PiicoDev_ENS160._VAL_PART_ID,
    "PiicoDev_LIS3DH": PiicoDev_LIS3DH._ID,
    "PiicoDev_MPU6050": None,
    "PiicoDev_MS5637": None,
    "PiicoDev_Potentiometer": [
        PiicoDev_Potentiometer._DEVICE_ID_POT,
        PiicoDev_Potentiometer._DEVICE_ID_SLIDE,
    ],
    "PiicoDev_QMC6310": None,
    "PiicoDev_RFID": None,
    "PiicoDev_RGB": PiicoDev_RGB._DevID,
    # There is a register read, but not checked
    "PiicoDev_RV3028": None,
    "PiicoDev_Servo": None,
    "PiicoDev_SSD1306": None,
    "PiicoDev_Switch": PiicoDev_Switch._DEVICE_ID,
    "PiicoDev_TMP117": None,
    "PiicoDev_Transceiver": PiicoDev_Transceiver._DEVICE_ID,
    "PiicoDev_Ultrasonic": PiicoDev_Ultrasonic._DEVICE_ID,
    "PiicoDev_VEML6030": None,
    "PiicoDev_VEML6040": None,
    "PiicoDev_VL53L1X": 0xEACC,
}


def create_dev_id_map():
    ret = {}
    for dev_str, raw_v in DEVICE_IDS.items():
        if isinstance(raw_v, list):
            pass
        else:
            raw_v = [raw_v]

        for v in raw_v:
            if v is None:
                continue
            elif isinstance(v, bytes):
                v = int.from_bytes(v, "big")

            if v in ret:
                warnings.warn(
                    "Multiple devices share the same ID, this has not been tested",
                    UserWarning,
                )
                # Duplicate device ID
                if not isinstance(ret[v], list):
                    ret[v] = [ret[v]]
                ret[v].append(dev_str)
            else:
                ret[v] = dev_str
    return ret


DEV_ID_MAP = create_dev_id_map()

DEV_ID_TYPE_MAP = {
    _k: STR_TO_TYPE_MAP[_v]
    for _k, _v in DEV_ID_MAP.items()
    if isinstance(_v, str) and _v in STR_TO_TYPE_MAP
}

if __name__ == "__main__":

    def _main():
        """
        Runs a little report used for planning
        """
        default_address_report = []
        print("Default addresses:")
        used_addresses = set()
        for dev_str, a in DEFAULT_ADDRESSES.items():
            if a in used_addresses:
                default_address_report.append(f"{a:04}(D) {dev_str}")
            else:
                default_address_report.append(f"{a:04}    {dev_str}")
                used_addresses.add(a)

        default_address_report.sort()
        for l in default_address_report:
            print(l)

        print("\n\nDEV_ID_MAP")
        id_map_report = []
        for dev_id, dev_strs in DEV_ID_MAP.items():
            id_map_report.append(
                f"{dev_id:04x}    {dev_strs}    {DEV_ID_TYPE_MAP.get(dev_id)}"
            )
        id_map_report.sort()
        for l in id_map_report:
            print(l)

    _main()
