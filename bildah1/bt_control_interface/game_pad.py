#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import pathlib
from typing import Dict, Optional

from evdev import InputDevice, ecodes, InputEvent

from bildah1.exceptions import BildahConnectionError, BildahCriticalConnectionError
from bildah1.log import EXTRA_DEBUG

logger = logging.getLogger(__name__)

abs_lookup = {}
for obj_name in dir(ecodes):
    if obj_name[:4] == "ABS_":
        val = getattr(ecodes, obj_name)
        abs_lookup[val] = obj_name


class GamePad:
    """
    This class processes EVDEV events captures the state of the
    GamePad.  It is intended to be subclassed for actions to be
    defined.  In addition, only one instance can be using the input
    at a time, so when an instance is being destroyed, it needs to explicitly
    call the close command.
    """

    # Used to remap code names for button handlers to improve readability
    remap_codes = {}
    connected_inputs = []

    @staticmethod
    def get_interface() -> Optional[str]:
        found = None
        for p in pathlib.Path("/dev/input").glob("event*"):
            logger.info(f"Found event interface: {p}")
            found = str(p)
        return found

    def __init__(self):
        logger.info("Looking for controllers")

        found = self.get_interface()

        self.input_dev = found
        self.state = {}
        self.key_aliases = {}

        if not found:
            raise BildahConnectionError(
                "Could not find a gamepad",
                recommended_wait=3000,
            )
        elif found in GamePad.connected_inputs:
            raise RuntimeError(f"GamePad instance already connected to {found}")

        try:
            self.gamepad = InputDevice(found)
        except OSError as e:
            raise BildahCriticalConnectionError(
                f"An OSError Occurred trying to connect to the input device: {e}",
            )

    def close(self):
        """
        Cleans up the instance explicitly, allowing other instances to access the input device.
        """
        if self.input_dev is not None:
            logger.debug(f"GamePad Removing reference to %s", self.input_dev)
            if self.input_dev in GamePad.connected_inputs:
                GamePad.connected_inputs.remove(self.input_dev)

    def __del__(self):
        """
        Note that this function might not be gaurunteed to be called by the interpreter.  It is
        best to explicitly call close.
        """
        self.close()

    def read_loop(self):
        """
        This function will continually read events associated with
        this input device.  At every event:

        * It will sync state to represent the last known value of a
          given button.

        * It will call any class handlers.

        * It will yield the event for further customisations before the next loop.
        """
        try:
            for event in self.gamepad.read_loop():
                old_values = self._update_state(event)
                event_handled = False
                for corrected_key_name, old in old_values.items():
                    if self._handle_state_change(
                        corrected_key_name, old, self.state[corrected_key_name]
                    ):
                        event_handled = True
                    else:
                        pass
                if not event_handled and old_values:
                    logger.debug(
                        "No handler defined for: %s",
                        old_values.keys(),
                    )
                yield event
        except Exception as e:
            logger.exception(e)
            raise BildahConnectionError(
                f"An unhandled error occurred in the read loop, see logs for details",
                recommended_wait=3000,
            )

    def _update_state(self, event: InputEvent) -> Dict[str, int]:
        """
        Call with an Input Event and this object will update accordingly.

        This returns the old values of all keys updated.
        """
        event_type = event.type
        old_values = {}

        if event_type == ecodes.EV_SYN:
            if EXTRA_DEBUG:
                logger.debug("A sync event occurred (our logic will ignore these)")
        elif event_type == ecodes.EV_MSC:
            if EXTRA_DEBUG:
                logger.debug("A misc event occurred (our logic will ignore these)")
        elif event_type == ecodes.EV_KEY or event_type == ecodes.EV_ABS:
            if event_type == ecodes.EV_ABS:
                raw_event_key = abs_lookup[event.code]
            else:
                raw_event_key = ecodes.keys[event.code]

            if hasattr(raw_event_key, "startswith"):
                event_keys = [raw_event_key]
            else:
                event_keys = raw_event_key

            if EXTRA_DEBUG:
                logger.debug("Key event received:  %s, %s", event_keys, event)

            for k in event_keys:
                corrected_key_name = self.remap_codes.get(k) or k
                old_values[corrected_key_name] = self.state.get(corrected_key_name)
                self.state[corrected_key_name] = event.value
        else:
            if EXTRA_DEBUG:
                logger.debug(f"Unhandled GamePad event: {event}")

        return old_values

    def _handle_state_change(self, corrected_key_name, old_value, new_value) -> bool:
        btn_function_name = f"handle_{corrected_key_name.lower()}"
        if hasattr(self, btn_function_name):
            try:
                ret = getattr(self, btn_function_name)(old_value, new_value)
                if not ret:
                    logger.debug(
                        "Handler %s did not return success for %s -> %s",
                        btn_function_name,
                        old_value,
                        new_value,
                    )
            except Exception as e:
                logger.warning(
                    "Handler %s raised an unhandled exception", btn_function_name
                )
                logger.exception(e)
            return True
        else:
            return False


class EightBitDoZero2(GamePad):
    BTN_X = "BTN_X"
    BTN_Y = "BTN_Y"
    BTN_B = "BTN_B"
    BTN_A = "BTN_A"
    BTN_L = "BTN_L"
    BTN_R = "BTN_R"
    BTN_SELECT = "BTN_SELECT"
    BTN_START = "BTN_START"

    remap_codes = {
        "BTN_C": BTN_Y,
        "BTN_A": BTN_B,
        "BTN_B": BTN_A,
        "BTN_Y": BTN_L,
        "BTN_Z": BTN_R,
        "BTN_TL": BTN_SELECT,
        "BTN_TR": BTN_START,
    }

    # These are calculated values just for posterity
    X_LEFT = 0
    X_CENTRE = 2**15  # 32768
    X_RIGHT = 2**16 - 1  # 65535

    Y_DOWN = 2**16 - 1  # 65535
    Y_CENTRE = 2**15  # 32768
    Y_UP = 0

    def handle_btn_x(self, old_value, new_value):
        pass

    def handle_btn_y(self, old_value, new_value):
        pass

    def handle_btn_b(self, old_value, new_value):
        pass

    def handle_btn_a(self, old_value, new_value):
        pass

    def handle_btn_l(self, old_value, new_value):
        pass

    def handle_btn_r(self, old_value, new_value):
        pass

    def handle_abs_x(self, old_value, new_value):
        pass

    def handle_abs_y(self, old_value, new_value):
        pass

    def handle_btn_select(self, old_value, new_value):
        pass

    def handle_btn_start(self, old_value, new_value):
        pass
