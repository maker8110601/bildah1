#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional

import motoron

from bildah1.hardware_spec_classes import MotoronDevInfo
from bildah1.i2c_bus.constants import PiicoDevUnifiedTypes, DEFAULT_STACK_POSITION
from bildah1.motoron_interface.constants import (
    JUMPER_STATE_MASK,
    JUMPER_STATE_LOOKUP,
    DEFAULT_DEVICE_NAME,
    DEFAULT_ADDRESS,
)


def test_device_address(bus: PiicoDevUnifiedTypes, address) -> Optional[MotoronDevInfo]:
    test = motoron.MotoronI2C(bus=bus.i2c, address=address)

    try:
        # Multiple Motoron devices on the same address sending different responses will
        # cause CRC errors, but we would like to ignore them.
        test.disable_crc_for_responses()
        v = test.get_firmware_version()
        jumper_state = test.get_jumper_state() & JUMPER_STATE_MASK
        v_in_mv = test.get_vin_voltage_mv()
        status = test.get_status_flags()

    except OSError:
        return None

    jumper_string = JUMPER_STATE_LOOKUP[jumper_state]

    dev = MotoronDevInfo(
        name=DEFAULT_DEVICE_NAME,
        stack_position=DEFAULT_STACK_POSITION,
        device=v["product_id"],
        address=address,
        is_address_set_to_default=address == DEFAULT_ADDRESS,
        firmware_version=f"version={v['firmware_version']['major']:x}.{v['firmware_version']['minor']:02x}",
        jumper_status=jumper_string,
        v_in_mv=v_in_mv,
        motor_faulting=bool(status & (1 << motoron.STATUS_FLAG_MOTOR_FAULTING)),
        no_power=bool(status & (1 << motoron.STATUS_FLAG_NO_POWER)),
        error_active=bool(status & (1 << motoron.STATUS_FLAG_ERROR_ACTIVE)),
        motor_output_enabled=bool(
            status & (1 << motoron.STATUS_FLAG_MOTOR_OUTPUT_ENABLED)
        ),
        motor_driving=bool(status & (1 << motoron.STATUS_FLAG_MOTOR_DRIVING)),
    )
    if dev.might_be_motoron():
        return dev
    else:
        return None
