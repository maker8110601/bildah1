"""
This package provides a convenient control interface for motoron control boards
"""
import logging
from typing import NoReturn

import cachetools
import motoron as _motoron

from bildah1.hardware_spec_classes import MotoronDevInfo

logger = logging.getLogger(__name__)


@cachetools.cached(cache=cachetools.LRUCache(maxsize=128), key=lambda dev: dev.address)
def get_motoron_i2c(dev: MotoronDevInfo) -> _motoron.MotoronI2C:
    """
    Creates and initialises an interfaces as per:

    https://core-electronics.com.au/guides/raspberry-pi/gobilda-motors-raspberry-pi-pololu-motoron/
    """
    interface = _motoron.MotoronI2C(address=dev.address)
    interface.reinitialize()
    # interface.disable_crc()
    interface.clear_reset_flag()

    logger.warning(
        "A command timeout is being applied, motors will stop automatically when "
        "no command is provided"
    )
    interface.set_command_timeout_milliseconds(dev.command_timeout_milliseconds)

    return interface


@cachetools.cached(cache=cachetools.LRUCache(maxsize=128), key=lambda dev: dev.address)
def get_motoron(dev: MotoronDevInfo) -> "MotoronControl":
    interface = get_motoron_i2c(dev=dev)

    motor = MotoronControl(
        motoron_dev_info=dev,
        motoron_i2c=interface,
        call_check=False,
    )

    return motor


class MotoronControl:
    """
    A simplified interface for a motoron control board (or set of boards)

    Some logic is derived from:

    https://core-electronics.com.au/guides/raspberry-pi/gobilda-motors-raspberry-pi-pololu-motoron/

    """

    def __init__(
        self,
        motoron_dev_info: MotoronDevInfo,
        motoron_i2c: _motoron.MotoronI2C,
        call_check=True,
    ):
        if call_check:
            raise ValueError(
                "This class should not be initialised directly, use get_motor instead."
            )

        self.motoron_i2c = motoron_i2c
        self.config: MotoronDevInfo = motoron_dev_info
        self.name = self.config.name

    def __str__(self):
        return f"Motoron Board:  {self.name}"

    def set_coast(self) -> NoReturn:
        """
        Sets all the motors to coast.
        """
        self.motoron_i2c.coast_now()
