#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module is a communication interface to the Bildah1 i2c bus interface.
"""
import asyncio
import logging

from bildah1.i2c_bus import validate_address, i2c_request
from bildah1.i2c_bus.device_scan import ping_all_addresses
from bildah1.log import enable_logging
from bildah1.motoron_interface.constants import (
    DEFAULT_ADDRESS,
    JMP1_STILL_CONNECTED_ADDRESS,
)
from bildah1.platform_utilities import async_sleep_ms

logger = logging.getLogger(__name__)

CHANGE_WAIT_MS = 500


async def reset_motoron(address: int):
    """
    This sends a soft reboot command to the Motoron.

    :raises Exception:  Other exceptions might be raised that are not handled here.
    :return: If an exception is not raised, then the command was completed okay.
    """
    await i2c_request(("motoron", "reset"), address)


async def set_motoron_address(from_address: int, to_address: int):
    """
    :raises ValueError: if there is a problem with one of the given addresses
    :raises RuntimeError: if the change did not occur as expected
    :raises Exception:  Other exceptions might be raised that are not handled here.
    :return: If an exception is not raised, the address has been set.
    """
    # Input validation
    validate_address(from_address)
    validate_address(to_address)
    existing_addresses = await ping_all_addresses(reset_caches=True)
    if to_address in existing_addresses:
        raise ValueError(f"Address {to_address} already exists.")
    elif from_address not in existing_addresses:
        raise ValueError(
            f"Address {from_address} does not exist and can not be changed."
        )

    if JMP1_STILL_CONNECTED_ADDRESS in existing_addresses:
        interim_pre_existed = True
        logger.warning(
            f"Address {JMP1_STILL_CONNECTED_ADDRESS} should be kept free as it is sometimes used by Motoron unexpectedly"
        )
    else:
        interim_pre_existed = False

    # Request Address Change
    await i2c_request(("motoron", "set_address"), from_address, to_address)
    await async_sleep_ms(CHANGE_WAIT_MS)
    await reset_motoron(from_address)
    await async_sleep_ms(CHANGE_WAIT_MS)
    existing_addresses = await ping_all_addresses(reset_caches=True)

    if from_address not in existing_addresses and to_address not in existing_addresses:
        if interim_pre_existed:
            # Nothing to do,
            raise RuntimeError(
                f"Motoron that was address {from_address} has disappeared.  "
                f"It might have need a power cycle, otherwise make sure no other device "
                f"is on address {JMP1_STILL_CONNECTED_ADDRESS}."
            )
        elif JMP1_STILL_CONNECTED_ADDRESS in existing_addresses:
            # Try several resets on the INTERIM device
            for i in (2, 5, 7, 11):
                logger.warning(
                    f"Trying to reset a motoron device, make sure that JMP1 is no longer connected to GND."
                )
                await reset_motoron(JMP1_STILL_CONNECTED_ADDRESS)
                await async_sleep_ms(i * CHANGE_WAIT_MS)
                existing_addresses = await ping_all_addresses(reset_caches=True)
                if JMP1_STILL_CONNECTED_ADDRESS in existing_addresses:
                    continue
                else:
                    break

    if from_address in existing_addresses:
        raise RuntimeError(
            f"Motoron at address {from_address} did not change, is JMP1 connected to GND?"
        )
    if to_address not in existing_addresses:
        if (
            not interim_pre_existed
            and JMP1_STILL_CONNECTED_ADDRESS in existing_addresses
        ):
            raise RuntimeError(
                f"Motoron that was at address {from_address} has changed, "
                f"JMP1 must no longer be connected to ground and the board needs to be reset."
            )
        else:
            raise RuntimeError(
                f"Motoron that was at address {from_address} has changed but not to the expected address {to_address}."
            )


# noinspection PyUnusedLocal
async def set_motoron_reinitialize(address: int):
    """
    This is recommended at system startup by Pololu.  It does not affect the
    address and is not the same as a reset.  It only updates tuning parameters
    of the motor control.

    :raises Exception:  Other exceptions might be raised that are not handled here.
    :return: If an exception is not raised, then the command was completed okay.
    """
    raise NotImplementedError


async def _main(from_address: int, to_address: int):
    """
    Used for prototyping and integration testing of address assignment logic
    """
    from bildah1.i2c_bus import run_i2c_thread

    # noinspection PyArgumentList
    async with run_i2c_thread():
        logger.info("Waiting for device scan")

        ret = await set_motoron_address(from_address, to_address)
        # ret = await reset_motoron(15)

        print(f"Set Address returned with: {ret}")


if __name__ == "__main__":
    enable_logging()
    # asyncio.run(_main(DEFAULT_ADDRESS, 22), debug=True)
    asyncio.run(_main(22, DEFAULT_ADDRESS), debug=True)
    # asyncio.run(_main(DEFAULT_ADDRESS, 91), debug=True)
