DEFAULT_ADDRESS = 16
JMP1_STILL_CONNECTED_ADDRESS = 15

DEFAULT_DEVICE_NAME = "motoron_device"
JUMPER_STATE_MASK = 3
JUMPER_STATE_ADDRESSING = "on"
JUMPER_STATE_LOOKUP = ["both", JUMPER_STATE_ADDRESSING, "off", "err"]

PRODUCT_MAP = {
    0x00CC: "M3S256, M3H256",
    0x00CD: "M2S, M2H",
    0x00CE: "M2T256",
    0x00CF: "M2U256",
    0x00D0: "M1T256",
    0x00D1: "M1U256",
    0x00D2: "M3S550, M3H550",
    0x00D3: "M2T550",
    0x00D4: "M2U550",
    0x00D5: "M1T550",
    0x00D6: "M1U550",
}
