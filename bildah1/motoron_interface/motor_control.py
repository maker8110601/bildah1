"""
This package provides a convenient control interface for individual motors.
"""

import logging
from typing import NoReturn

import cachetools
import motoron

from bildah1.hardware_spec_classes import MotoronDevInfo, MotoronMotorConfig
from bildah1.motoron_interface.motoron_control import get_motoron_i2c

logger = logging.getLogger(__name__)


@cachetools.cached(
    cache=cachetools.LRUCache(maxsize=128), key=lambda dev, motor: (dev.address, motor)
)
def get_motor(dev: MotoronDevInfo, motor: int) -> "MotorControl":
    """
    Creates and initialises an interfaces a motor

    https://core-electronics.com.au/guides/raspberry-pi/gobilda-motors-raspberry-pi-pololu-motoron/
    """
    interface = get_motoron_i2c(dev=dev)

    motor = MotorControl(
        motoron_dev_info=dev,
        motoron_i2c=interface,
        motor_number=motor,
        call_check=False,
    )

    return motor


class MotorControl:
    """
    A simplified interface for motor control.  This assumes all
    communications checks have been performed and that the I2C
    class is ready for use.

    This class is not expected to be created directly by users, but instead
    instantiated with get_motor.

    Some logic is derived from:

    https://core-electronics.com.au/guides/raspberry-pi/gobilda-motors-raspberry-pi-pololu-motoron/

    """

    MOTOR_SPEED_MIN = 0
    MOTOR_SPEED_MAX = 800

    def __init__(
        self,
        motoron_dev_info: MotoronDevInfo,
        motoron_i2c: motoron.MotoronI2C,
        motor_number: int = 1,
        call_check=True,
    ):
        if call_check:
            raise ValueError(
                "This class should not be initialised directly, use get_motor instead."
            )

        self.motoron_board_name = motoron_dev_info.name
        self.motor_number = motor_number
        self.motoron_i2c = motoron_i2c
        if self.motor_number == 1:
            self.config: MotoronMotorConfig = motoron_dev_info.motor_1
        elif self.motor_number == 2:
            self.config: MotoronMotorConfig = motoron_dev_info.motor_2
        else:
            raise ValueError(f"Unknown motor number {self.motor_number}")

        self.name = f"{self.motoron_board_name}.{self.config.name}"

        motoron_i2c.set_max_acceleration(motor_number, self.config.max_acceleration)
        motoron_i2c.set_max_deceleration(motor_number, self.config.max_deceleration)

    def __str__(self):
        return f"Motor:  {self.name}"

    def calculate_raw_speed(self, speed: float) -> int:
        """
        Calculate the raw speed given a value between -1 and 1.
        """
        speed_was_positive = speed >= 0
        clean_speed = min(1.0, abs(speed))

        min_raw_speed = self.MOTOR_SPEED_MIN
        max_raw_speed = min(self.MOTOR_SPEED_MAX, self.config.speed_limit)
        raw_speed_range = max_raw_speed - min_raw_speed
        raw_speed = clean_speed * raw_speed_range + min_raw_speed
        raw_speed = raw_speed if speed_was_positive else -1 * raw_speed
        return int(raw_speed)

    def set_speed_and_direction(self, speed: float, clockwise=True) -> NoReturn:
        """
        :param speed:  A value between 0 and 1 corresponding to the configured range of the motor.
        :param clockwise: False for counter-clockwise.
        """
        abs_speed = abs(speed)
        raw_speed = self.calculate_raw_speed(abs_speed if clockwise else -1 * abs_speed)

        logger.debug(
            "%s, Setting motor Speed to %0.2f  (Raw:  %s)", self.name, speed, raw_speed
        )
        self.motoron_i2c.set_speed(self.motor_number, int(raw_speed))

    def set_speed(self, speed: float) -> NoReturn:
        """
        :param speed:  A value between -1 and 1 corresponding to the configured range of the motor.
        """
        raw_speed = self.calculate_raw_speed(speed)

        logger.debug(
            "%s, Setting motor Speed to %0.2f  (Raw:  %s)", self.name, speed, raw_speed
        )
        self.motoron_i2c.set_speed(self.motor_number, int(raw_speed))
