import logging
import pathlib
import traceback

from evdev import InputDevice, ecodes

from bildah1.bt_control_interface.game_pad import abs_lookup, GamePad
from bildah1.log import enable_logging, print_to_console

logger = logging.getLogger(__name__)


def main():
    # print_to_console("Looking for controllers")
    #
    # found = None
    # for p in pathlib.Path("/dev/input").glob("event*"):
    #     print_to_console(f"Found event interface: {p}")
    #     found = p
    #
    # if not found:
    #     raise RuntimeError("Could not find a gamepad")
    #
    # gamepad = InputDevice(str(found))
    #
    # key_state = {}
    #
    # # This interface will run forever and simply print out key information
    # for event in gamepad.read_loop():
    #     event_type = event.type
    #
    #     match event_type:
    #         case ecodes.EV_SYN:
    #             print("A sync event occurred (our logic will ignore these)")
    #         case ecodes.EV_MSC:
    #             print("A misc event occurred (our logic will ignore these)")
    #         case ecodes.EV_KEY | ecodes.EV_ABS:
    #             if event_type == ecodes.EV_ABS:
    #                 raw_event_key = abs_lookup[event.code]
    #             else:
    #                 raw_event_key = ecodes.keys[event.code]
    #
    #             if hasattr(raw_event_key, "startswith"):
    #                 event_keys = [raw_event_key]
    #             else:
    #                 event_keys = raw_event_key
    #
    #             for k in event_keys:
    #                 key_state[k] = event.value
    #                 print(
    #                     f"A key was pressed: {event.code}({k})={event.value}  ({type(event.value)})"
    #                 )
    #         case _:
    #             print(f"Unknown event: {event}")
    #
    #     if key_state.get("BTN_TR"):
    #         break

    gp = GamePad()
    for _ in gp.read_loop():
        pass


if __name__ == "__main__":
    print_to_console(
        "This tool runs forever.  "
        "It will output key updates.  If a key has multiple typical names, it will report an event per possible name.\n"
        "Press CTRL-C to abort."
    )
    enable_logging()
    try:
        main()
    except Exception as e:
        print("Uncaught exception:", e)
        traceback.print_exc()
