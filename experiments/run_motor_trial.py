import logging
import time
import traceback

from app.basic_remote_control.hardware_spec import motoron_devices
from bildah1.log import enable_logging
from bildah1.motoron_interface.motor_control import get_motor

logger = logging.getLogger(__name__)


def main():
    bottom_motoron = motoron_devices[0]
    rear_left = get_motor(bottom_motoron, 1)

    # rear_right = get_motor(bottom_motoron, 2)

    logger.info("Setting motor speed")
    rear_left.set_speed_and_direction(0.2)
    time.sleep(10)
    logger.info("Setting motor speed again")
    rear_left.set_speed_and_direction(0.4)
    time.sleep(15)


if __name__ == "__main__":
    enable_logging()
    try:
        main()
    except Exception as e:
        print("Uncaught exception:", e)
        traceback.print_exc()
