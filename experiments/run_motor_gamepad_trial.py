import logging
import traceback

from app.basic_remote_control.hardware_spec import motoron_devices
from bildah1.bt_control_interface.game_pad import EightBitDoZero2
from bildah1.log import enable_logging
from bildah1.motoron_interface.motor_control import get_motor
from bildah1.motoron_interface.motoron_control import get_motoron

logger = logging.getLogger(__name__)


def main():
    bottom_motoron = motoron_devices[0]
    rear_motors = get_motoron(bottom_motoron)

    try:
        rear_left = get_motor(bottom_motoron, 1)

        # rear_right = get_motor(bottom_motoron, 2)

        exit_flag = False

        class TrialControl(EightBitDoZero2):
            def handle_btn_start(self, old_value, new_value):
                if old_value == 1 and new_value == 0:
                    nonlocal exit_flag
                    exit_flag = True
                    return True

            def handle_btn_x(self, old_value, new_value):
                nonlocal rear_left
                if new_value == 1:
                    rear_left.set_speed_and_direction(0.2)
                    return True

            def handle_btn_b(self, old_value, new_value):
                nonlocal rear_left
                if new_value == 1:
                    rear_left.set_speed_and_direction(0.5)
                    return True

            def handle_btn_y(self, old_value, new_value):
                nonlocal rear_left
                if new_value == 1:
                    rear_left.set_speed_and_direction(1.0)
                    return True

            def handle_btn_a(self, old_value, new_value):
                nonlocal rear_left
                if new_value == 1:
                    rear_left.set_speed_and_direction(0)
                    return True

        game_pad = TrialControl()

        for _ in game_pad.read_loop():
            if exit_flag:
                break
    except Exception as e:
        logger.exception(e)

    rear_motors.set_coast()


if __name__ == "__main__":
    enable_logging()
    try:
        main()
    except Exception as e:
        print("Uncaught exception:", e)
        traceback.print_exc()
