#!/bin/bash

# Courtesy of ChatGPT with some minor modifications.
# OpenAI. (2024). ChatGPT [Large language model]. https://chat.openai.com

# Define the service file path
SERVICE_FILE="/etc/systemd/system/bildah1.service"

# Function to create systemd service file
create_service_file() {
    cat << EOF > "$SERVICE_FILE"
[Unit]
Description=Run Bildah1 at startup
After=bluetooth.service

[Service]
Type=simple
User=bildah1
WorkingDirectory=/home/bildah1/app
ExecStart=/home/bildah1/.local/bin/poetry run python /home/bildah1/app/main.py

[Install]
WantedBy=multi-user.target
EOF
}

# Check for root access
if [ "$EUID" -ne 0 ]; then
  echo "This script must be run as root."
  exit 1
fi

# Check if the service file exists
if [ -f "$SERVICE_FILE" ]; then
    echo "Service file already exists."
else
    echo "Creating systemd service file for the Python app."
    create_service_file
    systemctl daemon-reload
    systemctl enable bildah1.service
    echo "Service file created and enabled."
fi
