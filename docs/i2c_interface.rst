=============
I2C Interface
=============


What is all this?
=================

It is complex (which is better than complicated, refer to `PIP20 <https://peps.python.org/pep-0020/>`_.

All I2C communication functions that communicate to the bus are in the _i2c_interface.py module.
These functions are not intended to be used directly by the Bildah1 app code.

All I2C processing are handled in a single dedicated communications thread in the _i2c_thread.py
module.  This thread is designed to be event driven to avoid arbitrary polling cycles.

Any function intended to be used outside of the i2c_bus package are made available in the
`__init__.py` module for that package.  These functions are async functions which will perform
a request for data and wait for a response using asyncio.

Doesn't this abstraction make it much harder to access vendor code?
-------------------------------------------------------------------

Yes,  unfortunately so.  However, tight-coupling is not considered highly maintainable either
for a complex app (and near-time real-world interfacing of a robot seems pretty complex).

Leaky abstraction is unavoidable (search Joel Spolsky's Law of Leaky Abstractions), the main
leak here is that the dependent libraries do not support concurrency models which, in their defence,
neither does the interface to the OS.  So adopting a concurrency model (asyncio) for the purpose of
having a much more maintainable multi-tasking robot app seems to outweigh the cost of
developing a loosely coupled interface.

Buy why?
--------

Why concurrency / asyncio / threads?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some prototyping indicated that I2C bus polling time is comparable to program execution time,
in other words, there is a good chance at least 50% of the time our program will be waiting
for I/O.

From a design perspective, that is the use case of `asyncio <https://en.wikipedia.org/wiki/Asynchronous_I/O>`_.

However, the libraries that perform communication use classical methods that do not support
asyncio, and probably never will.  The typical approach here is to use what is called an executor
to spin off blocking requests into a thread.  However, this is not supportable in micropython. It
can also be expensive to continually create threads (although threadpools are often used to reduce
this).

To emulate this, a single thread is being used for managing blocking requests so that the rest of
the app can continue to work while I/O operations are being performed.

Why event driven, why not just polling?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Probably stubbornness.  There is something seemingly more explicit with a knock on the door
compared with just opening the door ever 50ms and checking is someone is out the front.

Device Identification
=====================

PiicoDev Devices
----------------

These devices should be fairly easy to add, as the PiicoDev__Unified interface
is the main interface being used for generic I2C access.

To add a device:

* Open the PiicoDev Module for inspection in a Python IDE.  The best way to link to this
  is go to bildah1.piicodev_interface.identify_device and import the library.  Then use
  the IDE to navigate to the library.
* Make sure that the device is listed in bildah1.piicodev_interface.constants in the
  enumerator, DEV_ID_MAP, and DEV_ID_TYPE_MAP.  Read through the code of the PiicoDev module
  to find out its device / product ID.
* Then update device_tests in bildah1.piicodev_interface.identify_device.
* If customisations are needed, subclass in overrides.py so that there is a readID and readFirmware.


Motoron and other Devices
-------------------------

The same as the PiicoDev devices.

Performance Notes
=================

I have been quite fascinated with the performance implications of abstracting
the I2C I/O using a channels-like transport.  My expectation was that while the code
might appear complex, the overhead should be minimal.  It turns out that the overhead
of asyncio is comparably significant.

.. NOTE::
    TLDR;  While there is some latency induced of about 0.5 ms to 1 ms when sending
    a request to a thread (most of the latency appears to be to do with asyncio
    rather than the code here), there *should* be the significant benefit that the main
    thread is not blocked.  In other words, this only measures round trip latency,
    not the amount of time the thread is spend I/O bound.  Whereas if the I2C calls
    were made in the main thread, the whole program would freeze during an I/O operation
    (I/O blocking).  The I2C bus is very quick as it is, so the abstraction might
    be hard to justify considering how much complexity has been added.  As the project
    matures, this will be reviewed.

Very rough testing has been performed in async_smbus.py and _i2c_thread.py.



.. NOTE::
    In hindsight, the measurements here are not taking looking at what the impact
    is to the main loop in terms of blocking, only what the round trip communication
    latency is.

The rough results:

* Calling the sum function on 1000 numbers 1000 times took between 10 and 70 ms on a RPI4
* A Read poll request on the i2c bus was around 2ms
* Abstracting the interface with an event queue adds about 0.7ms of latency which seems mostly
  to come from the EPollSelector.  This can be reduced to about 0.2ms of latency if
  the SimpleAsyncResponseQueue does not used an async event, but instead releases control with
  an asyncio.sleep(0) command before looping.
* It is unclear why this adds so much latency, profiling indicate most of the time is spent
  in Asyncio.  This might have something to do with communication between threads as well as
  time spent waiting for the lock.

I also performed some performance testing with PyPy, after installing PyPy3 (pypy3 and pypy3-dev) on the RPI, I set it
up as a remote interpreter after ``poetry env use /usr/bin/pypy3`` followed with ``poetry update``.

The results were:

* About 2.4 ms per transaction with a thread and still a lot slower than CPython without.
* There is a big speed up between runs, I wonder if there is a compiling issue with the async code?
* The first run was always the slowest, but it sped up on subsequent runs except for the test where
  we were using the Queues.

Note that runs were executed multiple times and numbers were generally stable.

It also seems the profiler is not capturing _i2c_dispatcher_thread runtime.

At this stage it seems the latency is to do with asyncio.  Several modifications to the Request and
Response Queues didn't make a difference.

What about if the actual "task" took 2ms:

* 3.2ms per iteration via Thread, 2.2ms without thread, 2.1ms without async

What if the requests are all sent at once?

* 2.7ms per iteration via Thread, 2.7ms without thread,2.1 with neither

In the above test I needed to drop iterations down to 500 as a OSError was occurring.  As suspected
earlier, there is something happening with thread communication that adds latency.  This does not
occur when using the non thread option in the _overhead_check function.

When using the linux command ``time``::

    # On the RPI - each command run 5 times to check for consistency (they were consistent)
    cd ~/app
    poetry shell

    # Batch with Thread - System time increased because of thread communication
    /usr/bin/time python -m bildah1.i2c_bus._i2c_thread

        INFO        2024-01-09T07:33:34  _i2c_thread:220:    Total time duration is 1.35 seconds for 500 iterations
        INFO        2024-01-09T07:33:34  _i2c_thread:223:    Total time duration is 2.707 ms per iteration

        1.35user 0.35system 0:04.63elapsed 36%CPU (0avgtext+0avgdata 16296maxresident)k
        0inputs+0outputs (0major+2592minor)pagefaults 0swaps

    # Batch with async
    /usr/bin/time python -m bildah1.i2c_bus._i2c_thread

        INFO        2024-01-09T07:34:59  _i2c_thread:220:    Total time duration is 1.39 seconds for 500 iterations
        INFO        2024-01-09T07:34:59  _i2c_thread:223:    Total time duration is 2.770 ms per iteration

        0.53user 0.11system 0:04.67elapsed 13%CPU (0avgtext+0avgdata 14396maxresident)k
        0inputs+0outputs (0major+2134minor)pagefaults 0swaps

    # No batch, no async or thread
    /usr/bin/time python -m bildah1.i2c_bus._i2c_thread

        INFO        2024-01-09T07:35:48  _i2c_thread:220:    Total time duration is 1.05 seconds for 500 iterations
        INFO        2024-01-09T07:35:48  _i2c_thread:223:    Total time duration is 2.109 ms per iteration

        0.21user 0.09system 0:04.33elapsed 7%CPU (0avgtext+0avgdata 13292maxresident)k
        0inputs+0outputs (0major+1858minor)pagefaults 0swaps



