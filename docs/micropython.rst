===========
Micropython
===========

This document aims to summarise considerations needed for micropython
compatibility.

Known compatibility issues
==========================

So not to spend too much time working on Micropython support, this section
describes a few core areas where compatibility was broken but a rough plan
exists to bridge the gap.

Particularly since threading is still in its infancy in micropython:
https://docs.micropython.org/en/latest/library/_thread.html


simple_request_queue
--------------------

This class requires a threadsafe event.  Micropython has a ThreadSafeFlag
that could be used for this purpose, but this does not exist on Python.  The
threading.Event class will work fine on python.

What is interesting is that there is no thread safe "wait" currently available
except in the _thread module with a lock.  This will need to be explored further.


Standard Library Support
========================

By default, see here:
https://docs.micropython.org/en/latest/library/index.html#python-standard-libraries-and-micro-libraries

In addition, the following can be used to add more standard support:

https://github.com/micropython/micropython-lib

For runtime configuration, mip can be used in code as::

    mip.install("pathlib")

Note:  I am uncertain if this is meant to prefix the package with a namespace, I have
not tested this yet.

I2C
===

There are a number of libraries that work between smbus2 for RPI and then machine.I2C for
micropython.  smbus2 is meant to be pure python so there might be an opportunity to create an
smbus2 compatible interface for Machine.I2C (although very different implementation as smbus2
uses OS features of Linux).
