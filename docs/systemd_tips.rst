============
systemd Tips
============

Startup Timing
==============

Use ``systemd-analyze blame``


Shutdown Sequences
==================

You might need to install graphviz:  ``sudo apt install graphviz``

Use ``systemd-analyze dot 'shutdown.target' | dot -Tpng > shutdown.png``

Then, if using PyCharms, you can browse the remote host to open the
PNG file.

Writing Service Files
=====================

ChatGPT 4 is really handy for this.

