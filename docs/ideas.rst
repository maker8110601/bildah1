=====
Ideas
=====

This is a little area for capturing ideas that have come up along the way.

* Design electronics enclosures with some kind of suspension system.
* Add "License" Name Plate
* Add Headlights
* Add manual start up and handover power distribution to a small control board, that
  when the RPI stops responding will disconnect power to the whole system.
* Allow handle functions to return the state value so that data processing can occur in state
  (instead of return True).
