# A control program for my families robot called Bildah1
#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
import logging
import time
import traceback

from app import get_dev_by_name
from app.basic_remote_control.acorn_sprout_control import AcornSproutControl
from app.basic_remote_control.constants import (
    MOTOR_REAR_LEFT,
    MOTOR_REAR_RIGHT,
    MOTOR_FRONT_LEFT,
    MOTOR_FRONT_RIGHT,
)
from app.basic_remote_control.hardware_spec import motoron_devices
from bildah1.bt_control_interface.game_pad import GamePad
from bildah1.exceptions import BildahConnectionError
from bildah1.hardware_spec_classes import PiicoDevInfo
from bildah1.i2c_bus.device_scan import scan_for_devices, refresh_devices
from bildah1.log import enable_logging
from bildah1.motoron_interface.motor_control import get_motor
from bildah1.motoron_interface.motoron_control import get_motoron
from bildah1.piicodev_interface.rgb import (
    run_light_sequence,
    OnOffSequence,
    leds_on,
    CosineWaveSequence,
)
from bildah1.platform_utilities import async_sleep_ms
from bildah1.platform_utilities.colour_conversion import hsv_to_rgb
from bildah1.rpi_system import shutdown_system
from license_notices import print_general_terminal_notice

print_general_terminal_notice(__file__)

logger = logging.getLogger(__name__)

CONTROL = AcornSproutControl

# Assumes h 0 -> 360, and the other two 0 -> 1
ini_check_colour_r_g_b = hsv_to_rgb(*[213, 0.85, 1])
operating_colour_r_g_b = hsv_to_rgb(*[125, 1, 1])
warning_colour_r_g_b = hsv_to_rgb(*[32, 1, 1])
shutting_down_colour_r_g_b = hsv_to_rgb(*[0, 1, 1])


def main():
    bottom_motoron = motoron_devices[0]
    top_motoron = motoron_devices[1]
    rear_motors = get_motoron(bottom_motoron)
    front_motors = get_motoron(top_motoron)

    game_pad = CONTROL()

    game_pad.motor_controls[MOTOR_REAR_LEFT] = get_motor(bottom_motoron, 1)
    game_pad.motor_controls[MOTOR_REAR_RIGHT] = get_motor(bottom_motoron, 2)
    game_pad.motor_controls[MOTOR_FRONT_LEFT] = get_motor(top_motoron, 1)
    game_pad.motor_controls[MOTOR_FRONT_RIGHT] = get_motor(top_motoron, 2)
    game_pad.motoron_boards = [rear_motors, front_motors]

    for _ in game_pad.read_loop():
        if game_pad.exit_flag.is_set():
            break

    # game_pad.close()
    rear_motors.set_coast()
    front_motors.set_coast()

    if game_pad.shutdown_flag.is_set():
        shutdown_system()


hello_has_been_run_before = False


async def run_hello_sequence(main_led, repeats=3):
    global hello_has_been_run_before
    if hello_has_been_run_before:
        return
    else:
        hello_has_been_run_before = True

    # Say Hello
    await run_light_sequence(
        main_led,
        sequence_0=OnOffSequence(
            [
                (True, 600),
                (False, 600),
            ],
            repeat=repeats,
            rgb=ini_check_colour_r_g_b,
        ),
        sequence_1=OnOffSequence(
            [
                (False, 200),
                (True, 600),
                (False, 400),
            ],
            repeat=repeats,
            rgb=operating_colour_r_g_b,
        ),
        sequence_2=OnOffSequence(
            [
                (False, 400),
                (True, 600),
                (False, 200),
            ],
            repeat=repeats,
            rgb=shutting_down_colour_r_g_b,
        ),
    )


hardware_checking_sequence = OnOffSequence(
    [
        (True, 400),
        (False, 400),
    ],
    repeat=True,
    rgb=ini_check_colour_r_g_b,
)

hardware_checked_sequence = CosineWaveSequence(
    rgb=operating_colour_r_g_b,
)

hardware_error_sequence = OnOffSequence(
    [
        (True, 200),
        (False, 200),
    ],
    repeat=True,
    rgb=warning_colour_r_g_b,
)


positive_confirmation_wait_time = 1200


async def piicodev_hardware_check(main_led: PiicoDevInfo, led_number=0):
    """
    LED 0 will indicate Piicodev readiness.  There is no retry as this
    only tests if the hardware is installed correctly.
    """
    from app.i2c_config_wizard.piicodev_config import piicodev_config_check

    led_task = asyncio.create_task(
        run_light_sequence(
            main_led, **{f"sequence_{led_number}": hardware_checking_sequence}
        )
    )
    await async_sleep_ms(positive_confirmation_wait_time)

    # Stage 1:  Check PiicoDev (LED 0)
    dev_data = await scan_for_devices()
    error_count = await piicodev_config_check(dev_data, interactive=False)

    # noinspection DuplicatedCode
    led_task.cancel()
    try:
        await led_task
    except asyncio.CancelledError:
        pass

    if error_count == 0:
        led_task = asyncio.create_task(
            run_light_sequence(
                main_led, **{f"sequence_{led_number}": hardware_checked_sequence}
            )
        )
    else:
        logger.warning("Piicodev hardware is not ready.")
        led_task = asyncio.create_task(
            run_light_sequence(
                main_led, **{f"sequence_{led_number}": hardware_error_sequence}
            )
        )
    await async_sleep_ms(positive_confirmation_wait_time)

    return error_count, led_task


async def motoron_hardware_check(
    main_led: PiicoDevInfo, led_number=1, retries=5, retry_sleep_ms=3000
):
    """
    LED 1 will indicate motoron readiness.  This checks both hardware config and runtime.
    """
    from app.i2c_config_wizard.motoron_config import (
        motoron_runtime_check,
        motoron_config_check,
    )

    led_task = asyncio.create_task(
        run_light_sequence(
            main_led, **{f"sequence_{led_number}": hardware_checking_sequence}
        )
    )
    await async_sleep_ms(positive_confirmation_wait_time)

    dev_data = await scan_for_devices()
    error_count = await motoron_config_check(dev_data, interactive=False)

    runtime_errors = 0
    if not error_count:
        for _ in range(retries):
            runtime_errors = 0

            # Stage 2:  Check Motoron (LED 1)
            dev_data = await refresh_devices(dev_data)

            # No point in a runtime check if there are config issues
            errors_messages = motoron_runtime_check(dev_data)
            for r in errors_messages:
                logger.warning(r)
                runtime_errors += 1

            if runtime_errors:
                await async_sleep_ms(retry_sleep_ms)
            else:
                break
    error_count += runtime_errors

    # noinspection DuplicatedCode
    led_task.cancel()
    try:
        await led_task
    except asyncio.CancelledError:
        pass

    if error_count == 0:
        led_task = asyncio.create_task(
            run_light_sequence(
                main_led, **{f"sequence_{led_number}": hardware_checked_sequence}
            )
        )
    else:
        error_count += runtime_errors
        logger.warning("Motoron hardware is not ready.")
        led_task = asyncio.create_task(
            run_light_sequence(
                main_led, **{f"sequence_{led_number}": hardware_error_sequence}
            )
        )
    await async_sleep_ms(positive_confirmation_wait_time)

    return error_count, led_task


async def gamepad_hardware_check(
    main_led: PiicoDevInfo, led_number=2, retries=5, retry_sleep_ms=3000
):
    """
    LED 2 will indicate gamepad readiness
    """
    led_task = asyncio.create_task(
        run_light_sequence(
            main_led, **{f"sequence_{led_number}": hardware_checking_sequence}
        )
    )
    await async_sleep_ms(positive_confirmation_wait_time)

    gamepad_ok = False
    for _ in range(retries):
        if GamePad.get_interface():
            gamepad_ok = True
            break
        else:
            gamepad_ok = False
            await async_sleep_ms(retry_sleep_ms)

    # noinspection DuplicatedCode
    led_task.cancel()
    try:
        await led_task
    except asyncio.CancelledError:
        pass

    if gamepad_ok:
        led_task = asyncio.create_task(
            run_light_sequence(
                main_led, **{f"sequence_{led_number}": hardware_checked_sequence}
            )
        )
        error_count = 0
    else:
        logger.warning("The GamePad is not ready for startup")
        led_task = asyncio.create_task(
            run_light_sequence(
                main_led, **{f"sequence_{led_number}": hardware_error_sequence}
            )
        )
        error_count = 1

    await async_sleep_ms(positive_confirmation_wait_time)

    return error_count, led_task


def check_hardware_readiness() -> int:
    """
    Asyncio is still being trialled and is encapsulated here

    This assumes that the status_leds LED board is at least available

    :return: the number of errors found
    """
    error_count = 0

    async def _run():
        nonlocal error_count

        from bildah1.i2c_bus import run_i2c_thread, reset_i2c_thread

        reset_i2c_thread()

        # noinspection PyArgumentList
        async with run_i2c_thread():
            # LED sequences are intentionally complex for testing purposes

            main_led = get_dev_by_name("status_leds")

            await run_hello_sequence(main_led)

            check_tasks = [
                asyncio.create_task(piicodev_hardware_check(main_led)),
                asyncio.create_task(motoron_hardware_check(main_led)),
                asyncio.create_task(gamepad_hardware_check(main_led)),
            ]

            # Clean up
            tasks_results = await asyncio.gather(*check_tasks)
            error_count += sum(x[0] for x in tasks_results)
            led_tasks = asyncio.gather(*[x[1] for x in tasks_results])
            led_tasks.cancel()
            try:
                await led_tasks
            except asyncio.CancelledError:
                pass

            if error_count > 0:
                await leds_on(main_led, warning_colour_r_g_b)
            else:
                await leds_on(main_led, operating_colour_r_g_b)

        logger.debug(f"There were {error_count} startup errors")

    asyncio.run(_run(), debug=False)

    return error_count


if __name__ == "__main__":
    enable_logging()

    while True:
        _error_count = check_hardware_readiness()
        if _error_count:
            # check_hardware_readiness logs errors
            time.sleep(5)
            continue

        try:
            main()
        except BildahConnectionError as e:
            # If a BildahConnectionError has been raised, then assume logging and information
            # has occurred at the point of the exception
            if e.should_recover_on_soft_restart:
                logger.info(
                    "The bildah1 application is restarting and will sleep for %i ms",
                    e.recommended_wait,
                )
                time.sleep(e.recommended_wait / 1000.0)
            else:
                shutdown_system()
                break
        except Exception as _e:
            print("Uncaught exception:", _e)
            traceback.print_exc()
            shutdown_system()
            break
