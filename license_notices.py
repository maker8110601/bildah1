#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import pathlib

BASE_DIR = pathlib.Path(__file__).parent


def print_general_terminal_notice(module_path: str):
    p = pathlib.Path(module_path)
    mod_name = str(p.relative_to(BASE_DIR)).replace("/", ".")[:-3]

    print(
        f"""
    bildah1:{mod_name}  Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
        This program comes with ABSOLUTELY NO WARRANTY; for details see the COPYING file.
        This is free software, and you are welcome to redistribute it
        under certain conditions; see the COPYING file. for details.
    """
    )
