#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This script is used to configure i2c devices.
"""
import asyncio
from typing import List

import questionary

import app
from app.basic_remote_control.hardware_spec import MOTOR_VOLTAGE
from bildah1.hardware_spec_classes import MotoronDevInfo, I2cDeviceInfo
from bildah1.i2c_bus.device_scan import scan_for_devices, guess_device_identity
from bildah1.log import print_to_console, enable_logging
from bildah1.motoron_interface import constants, general


async def motoron_assembly_reminders():
    stack_check_q = questionary.confirm(
        "If motoron controllers are stacked, have the leads been trimmed and spacing screws applied as per the manual"
    )
    if not await stack_check_q.ask_async():
        print_to_console("Make sure to power off, clean up, and return when ready.")
        raise ValueError("The platform is not ready")


def check_found_motoron_devices(
    expected_dev: MotoronDevInfo, actual_dev: MotoronDevInfo, interactive=False
) -> bool:
    if expected_dev.device == actual_dev.device:
        print_to_console(
            f"Found Motoron Controller {expected_dev.name} ready at {expected_dev.address}",
            interactive,
        )
        return True
    else:
        print_to_console(
            f"Found Unexpected Motoron Controller for {expected_dev.name} as:  {str(actual_dev)}.",
            interactive,
        )
        return False


async def configure_new_motoron_device(
    dev_data: dict[int, I2cDeviceInfo], expected_dev: MotoronDevInfo
) -> bool:
    print_to_console(
        f"Did not find Motoron Controller for {expected_dev.name} at:  {expected_dev.address}"
    )
    default_device = dev_data.get(constants.DEFAULT_ADDRESS)

    if default_device and default_device.device == expected_dev.device:
        print_to_console(
            f"Found Compatible Motoron Controller at default address: {default_device.address}"
        )

        print_to_console("Caution:  This next step might reset all the i2c devices.")

        for i in range(3):
            if await questionary.confirm(
                f"Is JMP1 bridged?  Do you want to configure the controller as {expected_dev.name} at address {expected_dev.address}"
            ).ask_async():
                updated_device = await guess_device_identity(
                    constants.DEFAULT_ADDRESS, reset_caches=True
                )

                if updated_device.jumper_status == constants.JUMPER_STATE_ADDRESSING:
                    print_to_console(
                        f"Updating motoron Controller {expected_dev.name} at address {updated_device.address}, remove the JMP1 bridge after 2 seconds."
                    )
                    # noinspection PyBroadException
                    try:
                        await general.set_motoron_address(
                            constants.DEFAULT_ADDRESS, expected_dev.address
                        )
                        break
                    except Exception:
                        return False
                else:
                    print_to_console(
                        f"JMP1 is in the incorrect state: {updated_device.jumper_status}.  Try again (attempt {i})."
                    )

            else:
                print_to_console(f"Device {expected_dev.name} has not been configured.")
                return False

    else:
        print_to_console("No controller found, check the hardware and try again.")
        return False


async def motoron_config_check(
    dev_data: dict[int, I2cDeviceInfo], interactive=False
) -> int:
    """
    Used as a check-list to ensure appropriate motoron installation.

    Docs:

    * https://www.pololu.com/docs/0j84/all#3.2.4
    * https://www.pololu.com/docs/0j84/all#3.4

    Items that this list checks:

    * If controllers are being stacked, have the terminal leads been trimmed as to not interfere?

    :raises ValueError: If there is a configuration problem.
    """

    if interactive:
        await motoron_assembly_reminders()

    errors = 0

    for expected_dev in app.hardware_spec.motoron_devices:
        if expected_dev.address in dev_data:
            actual_dev = dev_data[expected_dev.address]
            if isinstance(actual_dev, MotoronDevInfo):
                # Found a motoron controller at the expected address
                if check_found_motoron_devices(expected_dev, actual_dev, interactive):
                    pass  # Everything checks out
                else:
                    errors += 1
            else:
                print_to_console(
                    f"A non motoron device found at {expected_dev.address}:  {expected_dev}",
                    interactive,
                )
                errors += 1
        elif interactive:
            if await configure_new_motoron_device(dev_data, expected_dev):
                pass  # Reconfiguring worked out
            else:
                errors += 1
        else:
            errors += 1

    return errors


def motoron_runtime_check(dev_data: dict[int, I2cDeviceInfo]) -> List[str]:
    """
    Performs online startup checks (assuming hardware config is okay).
    :return: A list of errors or an empty list if there are none
    """
    errors = []
    for expected_dev in app.hardware_spec.motoron_devices:
        actual_dev = dev_data[expected_dev.address]
        if actual_dev.motor_faulting:
            errors.append(f"{expected_dev.name}: The motor_faulting flag is set.")
        if actual_dev.no_power:
            errors.append(f"{expected_dev.name}: The no_power flag is set.")

        # This can also just indicate a command timeout or a reset, either way, it is probably
        # safe to ignore for now.
        # https://www.pololu.com/docs/0j84/all#var-error-mask
        # if actual_dev.error_active:
        #     errors.append(f"{expected_dev.name}: The error_active flag is set.")

        # Once an output occurs, this seems to always be on anyway, motor_driving is more reliable.
        # if actual_dev.motor_output_enabled:
        #     errors.append(
        #         f"{expected_dev.name}: the motors are expected to be stopped but the motor_output_enabled flag is set."
        #     )

        if actual_dev.motor_driving:
            errors.append(
                f"{expected_dev.name}: the motors are expected to be stopped but the motor_driving flag is set."
            )
        if actual_dev.v_in_mv < (0.8 * MOTOR_VOLTAGE * 1000):
            errors.append(
                f"{expected_dev.name}: The motor voltage is too low ({actual_dev.v_in_mv} mv)."
            )

    return errors


async def _run():
    from bildah1.i2c_bus import run_i2c_thread

    print_to_console(
        "Reminder:  If using Pycharm, make sure Emulate terminal in console is set "
        "in the run configuration"
    )
    # noinspection PyArgumentList
    async with run_i2c_thread():
        dev_data = await scan_for_devices()
        error_count = 0
        error_count += await motoron_config_check(dev_data, interactive=True)
        # for l in motoron_runtime_check(dev_data):
        #     print(l)

    print_to_console(
        f"There were {error_count} errors while configuring Motoron device"
    )


if __name__ == "__main__":
    enable_logging()
    asyncio.run(_run(), debug=True)
