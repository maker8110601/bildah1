#     Copyright (C) 2024  Gary Thompson <coding@garythompson.au>
#
#     This file is part of Bildah1.
#
#     Bildah1 is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This script is used to configure i2c devices.
"""
import asyncio

import questionary

import app
from bildah1.hardware_spec_classes import PiicoDevInfo, I2cDeviceInfo
from bildah1.i2c_bus.device_scan import scan_for_devices
from bildah1.log import print_to_console, enable_logging
from bildah1.piicodev_interface import constants, general


def check_found_piicodev_devices(
    expected_dev: PiicoDevInfo, actual_dev: PiicoDevInfo, interactive=False
) -> bool:
    if expected_dev.type == actual_dev.type:
        print_to_console(
            f"Found {actual_dev.type} {expected_dev.name} ready at {expected_dev.address}",
            interactive,
        )
        return True
    else:
        print_to_console(
            f"Found Unexpected {actual_dev.type} for {expected_dev.name} as:  {str(actual_dev)}.",
            interactive,
        )
        return False


async def configure_new_piicodev_device(
    dev_data: dict[int, I2cDeviceInfo], expected_dev: PiicoDevInfo
) -> bool:
    print_to_console(
        f"Did not find {expected_dev.type} for {expected_dev.name} at:  {expected_dev.address}"
    )
    default_address = constants.DEFAULT_ADDRESSES[expected_dev.type.value]
    default_device = dev_data.get(default_address)

    if default_device and default_device.type == expected_dev.type:
        print_to_console(
            f"Found {default_device.type} at default address: {default_device.address}"
        )

        if await questionary.confirm(
            f"Do you want to configure {default_device.type} as {expected_dev.name} at address {expected_dev.address}"
        ).ask_async():
            print_to_console(
                f"Updating {default_device.type} at address {default_device.address}."
            )
            # noinspection PyBroadException
            try:
                await general.set_piicodev_address(
                    default_device.type, default_address, expected_dev.address
                )
                return True
            except Exception:
                return False

        else:
            print_to_console(f"Device {expected_dev.name} has not been configured.")
            return False

    else:
        print_to_console("Not found, check the hardware and try again.")
        return False


async def piicodev_config_check(
    dev_data: dict[int, I2cDeviceInfo], interactive=False
) -> int:
    errors = 0

    for expected_dev in app.hardware_spec.piicodev_devices:
        if expected_dev.address in dev_data:
            actual_dev = dev_data[expected_dev.address]
            if isinstance(actual_dev, PiicoDevInfo):
                if check_found_piicodev_devices(expected_dev, actual_dev, interactive):
                    pass  # Everything checks out
                else:
                    errors += 1
            else:
                print_to_console(
                    f"A non PiicoDev device found at {expected_dev.address}:  {expected_dev}",
                    interactive,
                )
                errors += 1
        elif interactive:
            if await configure_new_piicodev_device(dev_data, expected_dev):
                pass  # Reconfiguring worked out
            else:
                errors += 1
        else:
            errors += 1

    return errors


async def _run():
    from bildah1.i2c_bus import run_i2c_thread

    print_to_console(
        "Reminder:  If using Pycharm, make sure Emulate terminal in console is set "
        "in the run configuration"
    )
    # noinspection PyArgumentList
    async with run_i2c_thread():
        dev_data = await scan_for_devices()
        error_count = await piicodev_config_check(dev_data, interactive=True)

    print_to_console(
        f"There were {error_count} errors while configuring Motoron device"
    )


if __name__ == "__main__":
    enable_logging()
    asyncio.run(_run(), debug=True)
