"""
Every app needs a hardware_spec, a control file, and optionally
a shutdown function.

Within the hardware_spec, will be a couple of lists of the different vendor
hardware intended for the robot.

The structure of the control file is still being decided.
"""
import warnings
from typing import Optional

from app.basic_remote_control import hardware_spec
from app.basic_remote_control.acorn_sprout_control import (
    AcornSproutControl as GamePad,
)
from app.basic_remote_control.shutdown import shutdown
from bildah1.hardware_spec_classes import I2cDeviceInfo


# hardware_spec Sanity checks
def check_hardware_spec():
    from bildah1.hardware_spec_classes import default_address_space
    import warnings

    used_addresses = set(x.address for x in hardware_spec.motoron_devices) | set(
        x.address for x in hardware_spec.piicodev_devices
    )

    if used_addresses & default_address_space:
        warnings.warn(
            "Some of the assigned addresses could conflict with future hardware"
        )


def _create_dev_map() -> dict:
    ret = {}
    for d in hardware_spec.motoron_devices + hardware_spec.piicodev_devices:
        if d.name in ret:
            warnings.warn(
                f"The hardware spec has a duplicate device name {d.name}.", UserWarning
            )
        else:
            ret[d.name] = d

    return ret


_dev_map = _create_dev_map()


def get_dev_by_name(name: str) -> Optional[I2cDeviceInfo]:
    return _dev_map[name]


def _create_dev_addr_map() -> dict:
    ret = {}
    for d in hardware_spec.motoron_devices + hardware_spec.piicodev_devices:
        if d.address in ret:
            warnings.warn(
                f"The hardware spec has a duplicate address {d}.", UserWarning
            )
        else:
            ret[d.address] = d

    return ret


device_address_map = _create_dev_addr_map()


check_hardware_spec()
