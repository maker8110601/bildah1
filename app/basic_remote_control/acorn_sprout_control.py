import logging
from enum import Enum

from app.basic_remote_control.acorn_control import AcornControl
from app.basic_remote_control.constants import (
    MOTOR_FRONT_LEFT,
    MOTOR_FRONT_RIGHT,
    MOTOR_REAR_LEFT,
    MOTOR_REAR_RIGHT,
)
from bildah1.python_compat import NumberType

logger = logging.getLogger(__name__)


class YMotion(Enum):
    FWD = "FWD"
    NONE = ""
    REV = "REV"


class XMotion(Enum):
    LEFT = "LEFT"
    NONE = ""
    RIGHT = "RIGHT"


# Note that this class is intended for only single use, so for
# simplicity, class variables will be used instead of a constructor.
class AcornSproutControl(AcornControl):
    """
    Acorn control is a simple control scheme that allows direct control
    of each motor.

    Each motor has a state of forward, stopped, reversed.

    Motor state is toggled as follows::

        Arrow Keys:  Fwd / Rev and Turn.
        Y/A:  Strafe left and right
        A/B:  No Action

    R/L, Start, and Select are preserved as per AcornControl.
    """

    min_speed = 0.2
    current_speed_setting = 0.4
    speed_increment = 0.1

    control_forward: YMotion = YMotion.NONE
    control_turn: XMotion = XMotion.NONE
    control_strafe: XMotion = XMotion.NONE

    def set_motor_state(self, motor: str, direction: int):
        """
        Overriden to make sure this is not accidentally used somewhere.
        """
        raise NotImplementedError(
            "This function will not be implemented for this class"
        )

    def refresh_motor_state(self):
        """
        Resends all current motor states
        """
        self.map_controls_to_motor()

    def map_controls_to_motor(self):
        """
        This function inspects the current control states and determines an action for all
        wheels.
        """
        fl_req: NumberType = 0
        fr_req: NumberType = 0
        rl_req: NumberType = 0
        rr_req: NumberType = 0

        fwd_mix = 1
        turn_mix = 1
        strafe_mix = 1

        logger.debug(
            "Mapping motor control:  Y=%s  X=%s  Strafe=%s",
            self.control_forward,
            self.control_turn,
            self.control_strafe,
        )
        match self.control_forward:
            case YMotion.FWD:
                fl_req += fwd_mix
                fr_req += fwd_mix
                rl_req += fwd_mix
                rr_req += fwd_mix
            case YMotion.REV:
                fl_req -= fwd_mix
                fr_req -= fwd_mix
                rl_req -= fwd_mix
                rr_req -= fwd_mix
            case _:
                pass

        match self.control_turn:
            case XMotion.LEFT:
                # Demand more from the right wheels
                fr_req += turn_mix
                rr_req += turn_mix
                fl_req -= turn_mix
                rl_req -= turn_mix
            case XMotion.RIGHT:
                # Demand more from the left wheels
                fr_req -= turn_mix
                rr_req -= turn_mix
                fl_req += turn_mix
                rl_req += turn_mix
            case _:
                pass

        match self.control_strafe:
            case XMotion.LEFT:
                # Split wheel demand across wheel sets
                fl_req -= strafe_mix
                fr_req += strafe_mix
                rl_req += strafe_mix
                rr_req -= strafe_mix
            case XMotion.RIGHT:
                # Demand more from the left wheels
                fl_req += strafe_mix
                fr_req -= strafe_mix
                rl_req -= strafe_mix
                rr_req += strafe_mix
            case _:
                pass

        logger.debug("Mix Request:  %s, %s, %s, %s", fl_req, fr_req, rl_req, rr_req)

        # TODO:  This needs some fixing
        max_request = max(fl_req, fr_req, rl_req, rr_req)
        min_request = min(fl_req, fr_req, rl_req, rr_req)
        request_range = max(1, max_request - min_request)
        normalise_request = max(abs(fl_req), abs(fr_req), abs(rl_req), abs(rr_req), 1)
        # logger.debug(
        #     "Control Mix based on: %s -> %s (%s)",
        #     min_request,
        #     max_request,
        #     request_range,
        # )
        # fl_speed = (fl_req / request_range + min_request) * self.current_speed_setting
        # fr_speed = (fr_req / request_range + min_request) * self.current_speed_setting
        # rl_speed = (rl_req / request_range + min_request) * self.current_speed_setting
        # rr_speed = (rr_req / request_range + min_request) * self.current_speed_setting
        fl_speed = (
            (fl_req / normalise_request)
            * self.current_speed_setting
            * (1 if self.MOTOR_CLOCKWISE_IS_FORWARD_MAP[MOTOR_FRONT_LEFT] else -1)
        )
        fr_speed = (
            (fr_req / normalise_request)
            * self.current_speed_setting
            * (1 if self.MOTOR_CLOCKWISE_IS_FORWARD_MAP[MOTOR_FRONT_RIGHT] else -1)
        )
        rl_speed = (
            (rl_req / normalise_request)
            * self.current_speed_setting
            * (1 if self.MOTOR_CLOCKWISE_IS_FORWARD_MAP[MOTOR_REAR_LEFT] else -1)
        )
        rr_speed = (
            (rr_req / normalise_request)
            * self.current_speed_setting
            * (1 if self.MOTOR_CLOCKWISE_IS_FORWARD_MAP[MOTOR_REAR_RIGHT] else -1)
        )

        logger.info(
            "Mix Speed:  %s, %s, %s, %s", fl_speed, fr_speed, rl_speed, rr_speed
        )

        self.motor_controls[MOTOR_FRONT_LEFT].set_speed(fl_speed)
        self.motor_controls[MOTOR_FRONT_RIGHT].set_speed(fr_speed)
        self.motor_controls[MOTOR_REAR_LEFT].set_speed(rl_speed)
        self.motor_controls[MOTOR_REAR_RIGHT].set_speed(rr_speed)

    def handle_abs_x(self, old_value, new_value):
        # Check button press
        match new_value:
            case self.X_LEFT:
                self.control_turn = XMotion.LEFT
            case self.X_CENTRE:
                self.control_turn = XMotion.NONE
            case self.X_RIGHT:
                self.control_turn = XMotion.RIGHT
            case _:
                logger.error("Unknown X Input: %s", new_value)
                self.exit_flag.set()
        self.map_controls_to_motor()
        return True

    def handle_abs_y(self, old_value, new_value):
        # Check button press
        match new_value:
            case self.Y_DOWN:
                self.control_forward = YMotion.REV
            case self.Y_CENTRE:
                self.control_forward = YMotion.NONE
            case self.Y_UP:
                self.control_forward = YMotion.FWD
            case _:
                logger.error("Unknown Y Input: %s", new_value)
                self.exit_flag.set()
        self.map_controls_to_motor()
        return True

    def handle_btn_x(self, old_value, new_value):
        # No action from the button
        return True

    def handle_btn_b(self, old_value, new_value):
        # No action from the button
        return True

    def handle_btn_y(self, old_value, new_value):
        # Check button press
        if new_value == 1:
            self.control_strafe = XMotion.LEFT
        else:
            self.control_strafe = XMotion.NONE
        self.map_controls_to_motor()
        return True

    def handle_btn_a(self, old_value, new_value):
        # Check button press
        if new_value == 1:
            self.control_strafe = XMotion.RIGHT
        else:
            self.control_strafe = XMotion.NONE
        self.map_controls_to_motor()
        return True
