import logging
import threading
from typing import Optional, Dict, List

from app.basic_remote_control.constants import (
    MOTOR_FRONT_LEFT,
    MOTOR_FRONT_RIGHT,
    MOTOR_REAR_LEFT,
    MOTOR_REAR_RIGHT,
    MOTOR_STOP,
    MOTOR_FWD,
    MOTOR_REV,
)
from bildah1.bt_control_interface.game_pad import EightBitDoZero2
from bildah1.motoron_interface.motor_control import MotorControl
from bildah1.motoron_interface.motoron_control import MotoronControl

logger = logging.getLogger(__name__)


# Note that this class is intended for only single use, so for
# simplicity, class variables will be used instead of a constructor.
class AcornControl(EightBitDoZero2):
    """
    Acorn control is a simple control scheme that allows direct control
    of each motor.

    Each motor has a state of forward, stopped, reversed.

    Motor state is toggled as follows::

        Left/Right:  Toggles the front left motor
        Up/Down:  Toggles the front right
        X/Y:  Toggles the rear left motor
        A/B:  Toggles the rear right motor

    In addition, motor speed can be toggled using the R/L buttons.

    Pressing select will stop all.

    To shut down the program and the raspberry PI:

    1.  Press and hold down select
    2.  Press and hold down start
    3.  Release start (it will shut down on release)

    """

    MOTOR_CLOCKWISE_IS_FORWARD_MAP = {
        MOTOR_FRONT_LEFT: False,
        MOTOR_FRONT_RIGHT: False,
        MOTOR_REAR_LEFT: False,
        MOTOR_REAR_RIGHT: False,
    }

    min_speed = 0.2
    current_speed_setting = min_speed
    speed_increment = 0.2

    current_motor_setting = {
        MOTOR_FRONT_LEFT: MOTOR_STOP,
        MOTOR_FRONT_RIGHT: MOTOR_STOP,
        MOTOR_REAR_LEFT: MOTOR_STOP,
        MOTOR_REAR_RIGHT: MOTOR_STOP,
    }

    exit_flag = threading.Event()  # Warning:  This should not be run in threads!
    shutdown_flag = threading.Event()  # Warning:  This should not be run in threads!

    # References to the motor control, these need to set after loading.
    motor_controls: Dict[str, Optional[MotorControl]] = {
        MOTOR_FRONT_LEFT: None,
        MOTOR_FRONT_RIGHT: None,
        MOTOR_REAR_LEFT: None,
        MOTOR_REAR_RIGHT: None,
    }

    motoron_boards: List[MotoronControl] = []

    def stop_all(self):
        for board in self.motoron_boards:
            board.set_coast()

    def set_motor_state(self, motor: str, direction: int):
        """
        Force a motor into a state
        """
        logger.debug("Set Motor State for %s:  %i", motor, direction)
        control = self.motor_controls[motor]

        assert control is not None, "Class not set up correctly, assign controllers"

        speed = abs(self.current_speed_setting)
        fwd_is_clockwise = self.MOTOR_CLOCKWISE_IS_FORWARD_MAP[motor]

        if direction == MOTOR_STOP:
            control.set_speed_and_direction(0)
        elif direction == MOTOR_FWD:
            control.set_speed_and_direction(speed, fwd_is_clockwise)
        else:
            # Reverse
            control.set_speed_and_direction(speed, not fwd_is_clockwise)

        self.current_motor_setting[motor] = direction

    def refresh_motor_state(self):
        """
        Resends all current motor states
        """
        for motor, direction in self.current_motor_setting.items():
            self.set_motor_state(motor, direction)

    # def toggle_motor_state(self, motor: str, direction: int):
    #     """
    #     Switch state depending on current state and direction input
    #     """
    #     current_state = self.current_motor_setting[motor]
    #     new_state = current_state + direction
    #     logger.debug(
    #         "Toggle Motor State for %s:  %i.  Current %i to new %i",
    #         motor,
    #         direction,
    #         current_state,
    #         new_state,
    #     )
    #     if new_state < 0:
    #         self.set_motor_state(motor, MOTOR_REV)
    #     elif new_state > 0:
    #         self.set_motor_state(motor, MOTOR_FWD)
    #     else:
    #         self.set_motor_state(motor, MOTOR_STOP)

    def clamp_and_update_speed(self):
        self.current_speed_setting = max(
            min(self.current_speed_setting, 1.0), self.min_speed
        )
        logger.debug("New Speed Setting: %0.2f", self.current_speed_setting)
        self.refresh_motor_state()

    def increase_speed(self):
        self.current_speed_setting += self.speed_increment
        self.clamp_and_update_speed()

    def decrease_speed(self):
        self.current_speed_setting -= self.speed_increment
        self.clamp_and_update_speed()

    def handle_btn_start(self, old_value, new_value):
        select_state = self.state.get(self.BTN_SELECT)

        logger.debug(
            "Shutdown sequence:  %s, %s, %s", old_value, new_value, select_state
        )
        # Check button release while select is active
        if old_value == 1 and new_value == 0 and select_state == 1:
            self.exit_flag.set()
            self.shutdown_flag.set()
        return True

    def handle_btn_select(self, old_value, new_value):
        logger.debug("Select Button Event:  %s -> %s", old_value, new_value)
        # Check button press
        if new_value == 1:
            self.stop_all()
        return True

    def handle_abs_x(self, old_value, new_value):
        # Check button press
        match new_value:
            case self.X_LEFT:
                self.set_motor_state(MOTOR_FRONT_LEFT, MOTOR_REV)
            case self.X_CENTRE:
                self.set_motor_state(MOTOR_FRONT_LEFT, MOTOR_STOP)
            case self.X_RIGHT:
                self.set_motor_state(MOTOR_FRONT_LEFT, MOTOR_FWD)
            case _:
                logger.error("Unknown X Input: %s", new_value)
                self.exit_flag.set()
        return True

    def handle_abs_y(self, old_value, new_value):
        # Check button press
        match new_value:
            case self.Y_DOWN:
                self.set_motor_state(MOTOR_FRONT_RIGHT, MOTOR_REV)
            case self.Y_CENTRE:
                self.set_motor_state(MOTOR_FRONT_RIGHT, MOTOR_STOP)
            case self.Y_UP:
                self.set_motor_state(MOTOR_FRONT_RIGHT, MOTOR_FWD)
            case _:
                logger.error("Unknown Y Input: %s", new_value)
                self.exit_flag.set()
        return True

    def handle_btn_x(self, old_value, new_value):
        # Check button press
        if new_value == 1:
            self.set_motor_state(MOTOR_REAR_LEFT, MOTOR_FWD)
        else:
            self.set_motor_state(MOTOR_REAR_LEFT, MOTOR_STOP)
        return True

    def handle_btn_y(self, old_value, new_value):
        # Check button press
        if new_value == 1:
            self.set_motor_state(MOTOR_REAR_LEFT, MOTOR_REV)
        else:
            self.set_motor_state(MOTOR_REAR_LEFT, MOTOR_STOP)
        return True

    def handle_btn_a(self, old_value, new_value):
        # Check button press
        if new_value == 1:
            self.set_motor_state(MOTOR_REAR_RIGHT, MOTOR_FWD)
        else:
            self.set_motor_state(MOTOR_REAR_RIGHT, MOTOR_STOP)
        return True

    def handle_btn_b(self, old_value, new_value):
        # Check button press
        if new_value == 1:
            self.set_motor_state(MOTOR_REAR_RIGHT, MOTOR_REV)
        else:
            self.set_motor_state(MOTOR_REAR_RIGHT, MOTOR_STOP)
        return True

    def handle_btn_l(self, old_value, new_value):
        # Check button press
        if new_value == 1:
            self.decrease_speed()
        return True

    def handle_btn_r(self, old_value, new_value):
        # Check button press
        if new_value == 1:
            self.increase_speed()
        return True
