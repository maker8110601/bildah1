"""
This module declares the intended hardware configuration for this app.

Note that the following i2c address spaces will be defined (inclusive ranges)
* 21 - 24:  Motor controllers (Motoron devices)
* 30 - 39:  PiicoDev Sensors
"""

from bildah1.hardware_spec_classes import (
    MotoronDevInfo,
    MotoronMotorConfig,
    PiicoDevInfo,
)
from bildah1.piicodev_interface.constants import PiicoDevType

MAX_MOTOR_SPEED = 400
MOTOR_VOLTAGE = 12

# For motoron device codes, see the motoron/scan.py file.
motoron_devices = [
    MotoronDevInfo(
        name="rear_wheels",
        stack_position=1,
        device=0x00CD,
        address=21,
        motor_1=MotoronMotorConfig(name="left", speed_limit=MAX_MOTOR_SPEED),
        motor_2=MotoronMotorConfig(name="right", speed_limit=MAX_MOTOR_SPEED),
    ),
    MotoronDevInfo(
        name="front_wheels",
        stack_position=2,
        device=0x00CD,
        address=22,
        motor_1=MotoronMotorConfig(name="left", speed_limit=MAX_MOTOR_SPEED),
        motor_2=MotoronMotorConfig(name="right", speed_limit=MAX_MOTOR_SPEED),
    ),
]

piicodev_devices = [
    PiicoDevInfo(
        type=PiicoDevType.RGB,
        name="status_leds",
        address=30,
        config={"brightness": 25},
    )
]
