MOTOR_FWD = 1
MOTOR_STOP = 0
MOTOR_REV = -1

MOTOR_FRONT_LEFT = "front_left"
MOTOR_FRONT_RIGHT = "front_right"
MOTOR_REAR_LEFT = "back_left"
MOTOR_REAR_RIGHT = "back_right"
