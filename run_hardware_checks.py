import asyncio
import logging

from bildah1.log import enable_logging
from license_notices import print_general_terminal_notice

print_general_terminal_notice(__file__)

print(
    "Make sure when using PyCharms remotely to set the configuration for this "
    "to emulate a terminal."
)

if __name__ == "__main__":
    enable_logging(level=logging.WARNING)

    async def _run():
        from bildah1.i2c_bus import run_i2c_thread
        from bildah1.i2c_bus.device_scan import scan_for_devices
        from app.i2c_config_wizard.piicodev_config import piicodev_config_check
        from app.i2c_config_wizard.motoron_config import motoron_config_check

        # noinspection PyArgumentList
        async with run_i2c_thread():
            dev_data = await scan_for_devices()
            error_count = await motoron_config_check(dev_data, interactive=True)
            error_count += await piicodev_config_check(dev_data, interactive=True)

        print(f"There were {error_count} errors while checking hardware configuration.")

    asyncio.run(_run(), debug=False)
