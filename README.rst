======
README
======

Welcome
=======

This is project "Bildah1" representing our first family exploration into robotics.

The goal of this is to create an experimentation bed using the GoBilda platform controlled with
a RaspberryPI.

.. NOTE::
    The main.py file is a mess as I am experimenting with how well asyncio can fit into
    this ecosystem and how to scaffold it to make it more accessible.

What skills do you need?
------------------------

The goal is to develop a project that is accessible to someone in upper primary school (someone
who is motivated to become reasonably strong at STEM).  Loading this project into a Raspberry PI
will need some support from someone who has some computer experience and willingness to have a go.
There are only hints here on the Raspberry PI initial setup, the internet will be your friend.

Once the Raspberry PI is setup then there will be some videos on how to setup the robot code on
the Raspberry PI.  There will also be some videos on how to setup PyCharms so that it can be used
to seamlessly tinker with the app code for the robot (the code in the app folder).

It is the app folder that is intended to be accessible.  All the scaffolding needed to
achieve this is in other folders.

What about advanced hacking of the project?
-------------------------------------------

The following Python skills and concepts are being applied:

* Multi-paradigm coding:  Object, procedural, functional, as needed.
* (In other words, somethings are imperative, some things are declarative)
* List and dictionary comprehensions
* Python Asyncio
* Python Threading (only very basic)
* Python decorators and context managers
* Python Logging
* Python Exceptions and Warnings
* Using getattr to dynamically access functions from a string
* Caching / memoization
* Event paradigm:  Having structures in place to avoid arbitrary polling
* Vendor libraries:  Motoron, PiicoDev.


Remix
=====

This projected started as a remix from the following guides, lots of thanks to Core-Electronics
for helping kick this off.  It has since evolved significantly.

* https://core-electronics.com.au/guides/prototyping/gobilda-chassis-kit-assembly-with-raspberry-pi/
* https://core-electronics.com.au/guides/raspberry-pi/gobilda-motors-raspberry-pi-pololu-motoron/

Our Robot is based off the following kit:

* https://core-electronics.com.au/strafer-chassis-kit-140mm-mecanum-wheels.html

Tech Stack
==========

Note that this is being built on a GNU/linux PC using PyCharm.  PyCharm will be able to deploy
and monitor scripts remotely on the RPI so that coding can occur on the PC.  For python management,
refer to poetry below.

For the actual electronics:

* RPI 4 Model B 2GB RAM

Poetry
------

`Poetry <https://python-poetry.org/>`_ is used for managing the python environment.  The easiest
way to install poetry is with pipx on a GNU/Linux PC with the command ``pipx install poetry``.

Directory Structure
-------------------

* 3d_prints - And files we create to go with our GoBilda chassis are here.
* app - this contains the robot's actual instructions and processing.  The code here
  is intended to be abstracted from the integration logic of the robot which is stored in bildah1.
* bildah1 - This contains the core Bildah1 generalised libraries and integration to support
  bildah1 configuration.
* deploy - some useful scripts for deploying bildah1 services on a RPI.
* docs - more information
* experiments - as the name suggests
* private - This is ignored by git, we use it for keeping low risk passwords for reference during development

Running the code
----------------

The main entry points are in the root directory.  These are:

* run_hardware_checks.py - a handy routine that inspects the robot for readiness.
* main.py - the file that runs the robot app

Multiple applications are stored in this project, each in the app directory.  To decide
which app is run, simply edit app.__ini__.py to import a different app.

Other tips
----------

Many of the Python modules can be run directly, just search for all files with the code
``if __name__ == "__main__":`` in it.  These are used for both testing and more low level
interaction such as adjusting addresses and so forth.

.. ATTENTION::
    There have been a lot of hurdles in making this a multi-tasking application.  See i2c_interface.rst
    for more details.

RPI Setup
=========

This project aims to be ROS2 compatible.  Unfortunately, they don't support Fedora well and
at the moment I will have to use Ubuntu LTS, which also seems better supported by rpi-imager.

rpi-imager (via the snap store) was used for the OS preparation on a fedora PC.  It allowed for
pre-configuration of the OS in the image which is summarised below.

* Ubuntu 22 (LTS) Server edition
* hostname bildah1
* default username  bildah1
* wireless LAN enabled for the main home network

For convenience of development, all instructions here are based on using SSH to access the RPI.
On initial install, your user will not be allowed access via the network with a password.  Enabling
passwords will be required at least initially (it can be disabled once you have ssh keys added
to the bildah1 user, describing how to do this is out of scope of this document but feel free to
look at how to use ``ssh-copy-id``).  To enable the initial use of ssh password::

    sudo nano /etc/ssh/sshd_config

    # Scroll to the bottom and add the following lines of text, make sure to include the
    # indent and replace "192.168.1.0" with your network subnet.

    Match address 192.168.21.0/24
        PasswordAuthentication yes

    # Press Ctrl-x to quit and press "Y" to save.
    # Now reload ssh

    sudo service ssh reload

    # Check for errors using:

    sudo service ssh status

I2C
---

To enable I2C without a gui:

* SSH to RPI
* ``sudo apt install raspi-config i2c-tools``
* ``sudo raspi-config``
* Follow the menu to enable I2C

RPI Dev Environment
-------------------

On the RPI via ssh.

* ``sudo apt install build-essential libssl-dev libffi-dev python3-dev cargo pkg-config git pipx gcc``
* ``pipx install poetry``

PC Dev Environment
------------------

* PyCharms (Tested on the professional version, might work on community)
* Set SSH host and PyCharm deployment configuration on the RPI in /home/bildah1/ with a project mapping to app.
* Set up automatic sync to the remote host in Tools -> Deployment
* Run poetry install remotely
* Set the PyCharm virtual environment to being the poetry env on the remote host (link to the poetry interpreter in ~/.cache/pypoetry/virtualenvs

Once PyCharm is setup, I found the following workflows useful:

* To update poetry:  Use poetry terminal on the RPI via SSH and then right click on pyproject.toml selecting to download from remote.
* To resync the interpreter after an update, just re-selecting the interpreter in the bottom right caused PyCharms to update to reflect the poetry changes.
* When right clicking the project in the project view, you can sync files between the project and the RPI

Bluetooth Controller
--------------------

The bluetooth is recognised first by the RPI Operating System and then our code uses standard OS
interfaces.

To pair a GamePad by ssh, we will interact with bluetoothctl.

In a terminal type:  ``bluetoothctl``

This will bring up an interactive interface to the bluetooth controller.

Type the command:  power on
(press enter)

Type the command:  scan on
(press enter)

You should now see a list of devices.  Look for your GamePad, mine appeared in the following line::

    [NEW] Device E4:17:D8:BF:09:F6 8BitDo Zero 2 gamepad

In that line is the MAC address for my GamePad:  E4:17:D8:BF:09:F6

Type the command (but use your MAC address):  pair E4:17:D8:BF:09:F6
(press enter)

Once it says pairing succeeded, then

Type the command (but use your MAC address):  connect E4:17:D8:BF:09:F6
(press enter)
Type the command (but use your MAC address):  trust E4:17:D8:BF:09:F6
(press enter)

Now a connection is complete.  We can turn off scanning.

Type the command:  scan off
(press enter)

To exit the bluetooth interface

Type the command:  exit
(press enter)

Finally, to enable

Passwordless Command Setup
--------------------------

On a linux machine, some commands require superuser privileges.  These commands will be useful
to run without these.  To do so:

Type the command:  ``EDITOR=nano sudo visudo /etc/sudoers.d/020_bildah1``

This brings up a simple text editor.  Add the following to the text editor::

    bildah1 ALL=(ALL) NOPASSWD: /sbin/shutdown
    bildah1 ALL=(ALL) NOPASSWD: /sbin/reboot
    bildah1 ALL=(ALL) NOPASSWD: /bin/dmesg
    bildah1 ALL=(ALL) NOPASSWD: /usr/bin/systemctl start bildah1.service
    bildah1 ALL=(ALL) NOPASSWD: /usr/bin/systemctl stop bildah1.service
    bildah1 ALL=(ALL) NOPASSWD: /usr/bin/systemctl enable bildah1.service
    bildah1 ALL=(ALL) NOPASSWD: /usr/bin/systemctl disable bildah1.service


Make sure to leave a blank space at the bottom of the file.



Other useful software on the Raspberry PI
-----------------------------------------

Cockpit is a useful remote management tool for handling users, software, and
managing services.  It can be installed with ``sudo apt install cockpit``.

Then added to run on startup with ``sudo systemctl enable --now cockpit.socket``

You can then access cockpit by pointing your browser to the address of your
RPI on port 9090:  https://192.168.1.10:9090

Note that you need to replace "192.168.1.10" with the IP address of your RPI and
that you will get a "Connection not private" error.  Click advanced to accept this.

Disabling unused services
-------------------------

Using Cockpit in the Services tab, the following services should be able to be disabled safely:

* snap.lxd.activate.service - needed for container management (you will know if you need this)
* lxd-agent.service - needed for container management (you will know if you need this)

In addition, the following can be uninstalled:

* ``sudo apt remove cloud-init``

Speeding up boot time
---------------------

Look at disabling unused services.

Waiting for networking takes a long time
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is an Ubuntu issue.  I'll skip the rant, but the following summarises it:

https://xkcd.com/927/

NetPlan or something has brought in both systemd-networkd as well as NetworkManager
which conflict with each other.  The rpi-imager seems to configure netplan as netplan
seems to be the Ubuntu standard.  It is important to note that it is not even v1 yet
and highly unstable, I am not sure why it is used in what is meant to be a server
environment.

In troubleshooting this, I made many changes.  I suspect the most important ones were::

    sudo apt remove network-manager

    # systemd-networkd-wait-online.service needs to be updated to look at wlan0

    sudo netplan set network.wifis.wlan0.optional=False

Reboot after changes to test.

I did see a bugreport for the RPI build somewhere.  Hopefully in the next LTS this will be easier.

Version planning and release information
========================================

Version 1 (acorn)
-----------------

Establish a direct wheel control mode.

Tasks below are only checked off after repeated testing.

[x] Bluetooth control starts at boot (tested by reboot)

[x] Button for all stop

[x] Button for shutdown OS (prep for power off)

[x] Buttons for each wheel fwd / stop / back

[x] Buttons for increase / decrease speed

[x] Optional:  All stop on loss of Gamepad (works because of an exception in the run loop)

As there is little user feedback in this setting, here are some tips:

* When the RPI is shutdown, there are no indicators.  However, if the
  gamepad controller does not connect then either the RPI is still booting (which currently
  takes a few minutes) or is in a shutdown mode.

* Make sure all files have been synchronised on the RPI

* Once a gamepad can connect then using PyCharm to remotely run run_acorn_low_level.py will work,
  alternatively, log in with ssh and use poetry to load in the Python environment with::

    cd app
    poetry run python ./run_hardware_checks.py
    poetry run python ./run_acorn_low_level.py.py

Make sure to check gear mesh tolerances as per the GoBilda instructions!

Version 1.1 (acorn_sprout)
--------------------------

This version provides an alternative control plan for buttons and button mapping

[x] Simpler control of the Mecanum wheels.

[x] If the gamepad fails, simply keep looping and gracefully recover

[x] Optional:  Auto start on power on

On the controller:

* Arrow keys combine to move and turn
* Y/A Keys strafe left and right

Enabling auto run requires logging in via SSH and executing the setup-service.sh as::

    cd /home/bildah1/app/deploy
    sudo ./setup-service.sh

Make sure to type in the bildah1 user password.

Now that the service is running on start, it is important to stop the service before
executing remotely with PyCharms.  This can be done with  ``sudo systemctl stop bildah1.service``.

In addition, to monitor the output of the program::

    journalctl -u bildah1.service --since "1 minute ago"
    journalctl -fu bildah1.service


Version 1.2 (acorn_sprout)
--------------------------

This version adds indication so that some basic troubleshooting can
occur without needing to look at logs.

[x] Optional:  Consider installing cockpit for remote diagnostics.

[x] Optional:  Hardware readiness can be an initialisation test as well as a wizard.

[x] Optional:  Investigate long startup time and remove spurious packages

[x] Check Motoron Readiness (Power available)

[x] Add PiicoDev LED for status indication

[x] Check controller readiness and report via LED

[x] Optional:  Investigate appropriate forms of concurrency

[ ] Make a service / script that keeps running right until the end that flashes the LEDs

On startup:

Led 0 represents PiicoDev readiness, led 1 is for Motoron, and led 2 is for the gamepad.

What is Version 2.0?
--------------------

Version 2 will see a refactor with a clearly defined concurrency model (for background processing,
such as asyncio) and a unified event driven interface (if it helps, something like PyDispatcher).
The intention is to improve abstraction from vendor specific nuances while better coupling the
logical processing from input, processing, to output.  The design will also need to ensure clear
output control (marshalling multiple demands).

Unfortunately, the async concurrency model is not implemented across a lot of key libraries (SMBus2)
and so additional work will be needed.  Version 2 might include a lot of monkey patching  as I don't
have time to write and maintain relevant libraries.

[ ] Add sensor and buzzer

[ ] Add a cabling diagram


Known Issues
------------

* On rescan of a motoron device, they seem to take address 15 until a power cycle has occurred.
  The hardware checks script does not perform properly as a result.


Future
------

* A useful gear / sensor / check routine where each output / input is cycled and confirmed with a report.

[ ] Add some unit tests for coverage on non I/O related functions (such as i2c_thread).

[ ] Optional:  Command sequence to cancel motor timeout

[ ] Optional:  Tune the mixing control to allow for more dominant buttons / modes.

[ ] Optional:  Command sequence to enter different settings (with LED / screen indication)

[ ] Readiness extra logging:  ``bluetoothctl paired-devices``

Bookmarks
=========

A list of interesting bookmarks that I encountered while researching.

* Threading on RPI PIco:  https://bytesnbits.co.uk/multi-thread-coding-on-the-raspberry-pi-pico-in-micropython/
* Core Electronics Guides:  https://core-electronics.com.au/guides/raspberry-pi/gobilda-motors-raspberry-pi-pololu-motoron/
* Motoron Manual:  https://www.pololu.com/docs/0j84/all#3.4
